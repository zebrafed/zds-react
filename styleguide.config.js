const path = require('path');

module.exports = {
  components: 'src/components/*/index.js',
  styleguideComponents: {
    Wrapper:
      process.env.DEMO === 'mobile'
        ? path.join(__dirname, 'src/styleguide/Wrapper.mobile')
        : path.join(__dirname, 'src/styleguide/Wrapper'),
  },
  getExampleFilename(componentPath) {
    return path.join(
      path.dirname(componentPath),
      'README' + (process.env.DEMO === 'mobile' ? '.mobile' : '') + '.md'
    );
  },
};

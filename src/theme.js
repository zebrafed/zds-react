//@flow
import { css } from 'styled-components';
type Color = string;
type Colors = string[];
type Swatches = { [number]: string };

const breakpoints = ['300px'];

const graySwatches: Swatches = {
  [900]: '#2E2E2E',
  [800]: '#4C4C4C',
  [700]: '#636363',
  [600]: '#757575',
  [500]: '#8E8E8E',
  [400]: '#A6A6A6',
  [300]: '#BDBDBD',
  [200]: '#CDCDCD',
  [100]: '#D7D7D7',
  [50]: '#E1E1E1',
  [10]: '#F5F5F5',
};

const navySwatches: Swatches = {
  [900]: '#1A237E',
  [800]: '#283593',
  [700]: '#303F9F',
  [600]: '#3949AB',
  [500]: '#3F51B5',
  [400]: '#5C6BC0',
  [300]: '#7986CB',
  [200]: '#9FA8DA',
  [100]: '#C5CAE9',
};

const cadetblueSwatches: Swatches = {
  [900]: '#263238',
  [800]: '#37474F',
  [700]: '#455A64',
  [600]: '#546E7A',
  [500]: '#607D8B',
  [400]: '#78909C',
  [300]: '#90A4AE',
  [200]: '#B0BEC5',
  [100]: '#CFD8DC',
};

const greenSwatches: Swatches = {
  [900]: '#33691E',
  [800]: '#558B2F',
  [700]: '#689F38',
  [600]: '#7CB342',
  [500]: '#8BC34A',
  [400]: '#9CCC65',
  [300]: '#AED581',
  [200]: '#C5E1A5',
  [100]: '#DCEDC8',
};

const tealSwatches: Swatches = {
  [900]: '#004D40',
  [800]: '#00695C',
  [700]: '#00796B',
  [600]: '#00897B',
  [500]: '#009688',
  [400]: '#26A69A',
  [300]: '#4DB6AC',
  [200]: '#80CBC4',
  [100]: '#B2DFDB',
};

const orangeSwatches: Swatches = {
  [900]: '#E65100',
  [800]: '#EF6C00',
  [700]: '#F57C00',
  [600]: '#FB8C00',
  [500]: '#FF9800',
  [400]: '#FFA726',
  [300]: '#FFB74D',
  [200]: '#FFCC80',
  [100]: '#FFE0B2',
};

const redSwatches: Swatches = {
  [900]: '#BF360C',
  [800]: '#D84315',
  [700]: '#E64A19',
  [600]: '#F4511E',
  [500]: '#FF5722',
  [400]: '#FF7043',
  [300]: '#FF8A65',
  [200]: '#FFAB91',
  [100]: '#FFCCBC',
};

const brownSwatches: Swatches = {
  [900]: '#3E2723',
  [800]: '#4E342E',
  [700]: '#5D4037',
  [600]: '#6D4C41',
  [500]: '#795548',
  [400]: '#8D6E63',
  [300]: '#A1887F',
  [200]: '#BCAAA4',
  [100]: '#D7CCC8',
};

export class Theme {
  lg: any = null;
  constructor(demo: boolean = false) {
    this.demo = demo;
    this.lg = (...args) => {
      return demo
        ? css`
            .demo-lg & {
              ${css(...args)};
            }
          `
        : css`
            @media (min-width: ${breakpoints[0]}) {
              ${css(...args)};
            }
          `;
    };
  }
  space = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 24, 26];
  fontWeights = [400, 500, 700];
  fontSizes = [14, 16, 18];
  primaryColor: Color = '#007ABA';
  dangerColor: Color = '#ED1C24';
  complementaryColor: Color = '#FFD200';
  neutralColors: Colors = ['#333E47', '#7E868C', '#DBD9D6'];
  secondaryColors: Colors = ['#00B400', '#00501E', '#FF8000', '#800000'];
  black: Color = '#000';
  white: Color = '#fff';
  graySwatches = graySwatches;
  navySwatches = navySwatches;
  cadetblueSwatches = cadetblueSwatches;
  greenSwatches = greenSwatches;
  orangeSwatches = orangeSwatches;
  redSwatches = redSwatches;
  tealSwatches = tealSwatches;
  brownSwatches = brownSwatches;
  breakpoints = breakpoints;
  demo: any;
}
export default new Theme();

export type ComponentWithTheme = {
  theme: Theme,
};

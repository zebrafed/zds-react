// @flow
import * as React from 'react';
import Flex from '@src/components/Common/Flex';
import Box from '@src/components/Common/Box';
import type { TextProps } from '@src/components/Common/Text';
import Text from '@src/components/Common/Text';
import styled from 'styled-components';
import type { ComponentType } from 'react';
import { MenuStyles } from '@src/components/Dropdown';
import { useRef, useCallback } from 'react';

export const SearchWrap: ComponentType<{ children: React.Node }> = styled(Flex).attrs({
  flex: 1,
  flexDirection: 'row',
  m: 4,
  justifyContent: 'center',
  alignItems: 'center',
})`
  & + ${MenuStyles} {
    top: 56px;
    border-top: 1px solid ${({ theme }) => theme.graySwatches[300]};
    width: auto;
    left: 8px;
    right: 8px;
  }
  position: relative;
  z-index: 1;
  box-sizing: border-box;
  background-color: ${({ theme }) => theme.white};
`;

export const SearchInput: ComponentType<any> = styled('input').attrs({
  p: 5,
  fontSize: 1,
})`
  background-color: ${({ theme }) => theme.white};
  display: block;
  outline: none;
  padding: ${({ theme }) => `0 ${theme.space[5]}px`};
  width: 100%;
  border: none;
  min-height: 48px;
  line-height: ${({ theme }) => theme.space[11]}px;
  color: ${({ theme }) => theme.graySwatches[900]};
  font-size: ${({ theme }) => theme.fontSizes[1]}px;

  ::placeholder {
    color: ${({ theme }) => theme.graySwatches[300]};
    font-size: ${({ theme }) => theme.fontSizes[1]}px;
    line-height: ${({ theme }) => theme.space[11]}px;
  }
`;

export const Label: ComponentType<{ children: React.Node }> = styled(Flex).attrs({
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
})``;

export const LeadingIcon: ComponentType<{ children: React.Node }> = styled(Flex)`
  max-width: 24px;
  max-height: 24px;
  margin-left: 8px;
  pointer-events: ${({ isFocused }) => (isFocused ? 'auto' : 'none')};
  cursor: pointer;
  position: relative;
  z-index: 3;

  ${Box}, svg {
    max-width: 24px;
    max-height: 24px;
    fill: ${({ theme }) => theme.graySwatches[500]};
    color: ${({ theme }) => theme.graySwatches[500]};
  }
`;

export const TrailingIcon: ComponentType<{ children: React.Node }> = styled(Flex)`
  max-width: 24px;
  max-height: 24px;
  margin-right: 8px;
  cursor: pointer;

  ${Box}, svg {
    fill: ${({ theme }) => theme.graySwatches[500]};
    color: ${({ theme }) => theme.graySwatches[500]};
  }
`;

type Props = {
  /**Content displayed on the left edge of the search box.*/
  leading: React.Node,
  /**Content displayed on the right edge of the search box.*/
  trailing: React.Node,
  /**The placeholder text in the search box.*/
  placeholder: string,
  /**The text entered in the search box .*/
  value?: string,
  /**A callback function that is fired when the enter key is pressed while the search box is in focus.*/
  onSubmit?: () => void,
  /**A callback function that is fired when the content of the searchbox is changed. */
  onChange?: (value: any) => void,
};

/**
 * Search boxes allow users to enter text into a UI and view a list of search suggestions.
 *
 * @example import { Search } from '@badger/react';
 */
const Search: ComponentType<Props> = ({
  leading,
  trailing,
  placeholder = '',
  value,
  onChange,
  onSubmit,
}: Props) => {
  const [isFocused, setFocused] = React.useState(false);

  const searchChange = (event: any) => {
    if (onChange) {
      onChange(event.target.value);
    }
    if (event.target.value.length > 0) {
      setFocused(true);
    } else {
      setFocused(false);
    }
  };

  const memoizedSearchChangeCallback = useCallback(searchChange, []);

  const searchSubmit = (event: any) => {
    event.preventDefault();
    if (onSubmit) {
      onSubmit();
    }
  };

  const memoizedSearchSubmitCallback = useCallback(searchSubmit, []);

  const leadingClick = (event) => {
    event.preventDefault();
    if (onSubmit) {
      onSubmit();
    }
  };

  const memoizedLeadingClickCallback = useCallback(leadingClick, []);

  return (
    <SearchWrap
      as={'form'}
      flex="1"
      flexDirection="column"
      justifyContent="center"
      ml={3}
      onSubmit={memoizedSearchSubmitCallback}
    >
      <Label as={'label'}>
        {leading && (
          <LeadingIcon isFocused={isFocused} onClick={memoizedLeadingClickCallback}>
            {leading}
          </LeadingIcon>
        )}
        <SearchInput
          type="text"
          placeholder={placeholder}
          value={value || ''}
          onChange={memoizedSearchChangeCallback}
        />
      </Label>
      {trailing && <TrailingIcon>{trailing}</TrailingIcon>}
    </SearchWrap>
  );
};
export default Search;

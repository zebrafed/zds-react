# Search with leading icon

A search box with a leading icon and no suggestions.

```js
import Search from '@badger/react/components/Search';
import SearchIcon from '@zebra/icons/general/icons/search.svg';

const [value, setValue] = React.useState('');

<Search
  leading={<SearchIcon width={24} height={24} />}
  onSubmit={() => console.log('submit')}
  onChange={setValue}
  value={value}
/>;
```

# Search with trailing icon

A search box with a trailing icon and no suggestions.

```js
import Search from '@badger/react/components/Search';
import { Ripple } from '@badger/react/components/Common';
import SearchIcon from '@zebra/icons/general/icons/search.svg';

const [value, setValue] = React.useState('');

<Search
  trailing={
    <Ripple onClick={() => console.log('submit')}>
      <SearchIcon width={24} height={24} />
    </Ripple>
  }
  onSubmit={() => console.log('submit')}
  onChange={setValue}
  value={value}
/>;
```

# Search with leading and trailing icon

A search box with a leading and trailing icon and no suggestions.

```js
import Search from '@badger/react/components/Search';
import { Icon, Ripple } from '@badger/react/components/Common';
import SearchIcon from '@zebra/icons/general/icons/search.svg';
import '@zebra/icons';

const [value, setValue] = React.useState('');

<Search
  leading={<SearchIcon width={24} height={24} />}
  onSubmit={() => console.log('submit')}
  onChange={setValue}
  value={value}
  trailing={
    <Ripple onClick={() => console.log('microphone')}>
      <Icon category={'general'} size={24} name={'microphone'} />
    </Ripple>
  }
/>;
```

# Search with leading icon and placeholder

A search box with a leading icon, some placeholder text and no suggestions.

```js
import Search from '@badger/react/components/Search';
import SearchIcon from '@zebra/icons/general/icons/search.svg';

const [value, setValue] = React.useState('');

<Search
  leading={<SearchIcon />}
  onChange={setValue}
  onSubmit={() => console.log('submit')}
  value={value}
  placeholder={'Search'}
/>;
```

# Search with trailing icon and placeholder

A search box with a trailing icon, placeholder text and no suggestions.

```js
import Search from '@badger/react/components/Search';
import { Ripple } from '@badger/react/components/Common';
import SearchIcon from '@zebra/icons/general/icons/search.svg';

const [value, setValue] = React.useState(null);

<Search
  trailing={
    <Ripple onClick={() => console.log('submit')}>
      <SearchIcon width={24} height={24} />
    </Ripple>
  }
  onSubmit={() => console.log('submit')}
  placeholder={'Search'}
  onChange={setValue}
  value={value}
/>;
```

# Search with leading and trailing icon and placeholder

A search box with a leading and trailing icon, some placeholder text and no suggestions.

```js
import Search from '@badger/react/components/Search';
import { Icon, Ripple } from '@badger/react/components/Common';
import SearchIcon from '@zebra/icons/general/icons/search.svg';
import '@zebra/icons';

const [value, setValue] = React.useState('');

<Search
  leading={<SearchIcon width={24} height={24} />}
  onChange={setValue}
  onSubmit={() => console.log('submit')}
  value={value}
  placeholder={'Search'}
  trailing={
    <Ripple onClick={() => console.log('microphone')}>
      <Icon category={'general'} size={24} name={'microphone'} />
    </Ripple>
  }
/>;
```

# Search with trailing icon prepopulated value and search suggestions

A search box with a trailing icon, a preopulated search value and three search suggestions. 

```js
import { Menu, MenuItem } from '@badger/react/components/Dropdown';
import { Ripple, Flex } from '@badger/react/components/Common';
import Search from '@badger/react/components/Search';
import SearchIcon from '@zebra/icons/general/icons/search.svg';

const [value, setValue] = React.useState('Pixel 3 phones');
<Flex position={'relative'} flexDirection="column">
  <Search
    trailing={
      <Ripple onClick={() => console.log('submit')}>
        <SearchIcon width={24} height={24} />
      </Ripple>
    }
    onSubmit={() => console.log('submit')}
    placeholder={'Search'}
    onChange={setValue}
    value={value}
  />
  <Menu visible={value && value.length > 3}>
    <MenuItem>{'Show all notification lorem ipsum dolor sit amet'}</MenuItem>
    <MenuItem>{'Hide sensitive lorem ipsum dolor sit amet'}</MenuItem>
    <MenuItem>{'Don’t show notifications at all'}</MenuItem>
  </Menu>
</Flex>;
```

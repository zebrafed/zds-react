# Mobile Search Example with leading icon

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import Search from '@badger/react/components/Search';
import SearchIcon from '@zebra/icons/general/icons/search.svg';

const [value, setValue] = React.useState('');

<Search
  leading={<SearchIcon width={24} height={24} />}
  onSubmit={() => console.log('submit')}
  onChange={setValue}
  value={value}
/>;
```

# Mobile Search Example with leading and trailing icon

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import Search from '@badger/react/components/Search';
import { Icon, Ripple } from '@badger/react/components/Common';
import SearchIcon from '@zebra/icons/general/icons/search.svg';
import '@zebra/icons';

const [value, setValue] = React.useState('');

<Search
  leading={<SearchIcon width={24} height={24} />}
  onSubmit={() => console.log('submit')}
  onChange={setValue}
  value={value}
  trailing={
    <Ripple onClick={() => console.log('microphone')}>
      <Icon category={'general'} size={24} name={'microphone'} />
    </Ripple>
  }
/>;
```

# Mobile Search Example with placeholder and leading icon

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import Search from '@badger/react/components/Search';
import SearchIcon from '@zebra/icons/general/icons/search.svg';

const [value, setValue] = React.useState('');

<Search
  leading={<SearchIcon />}
  onChange={setValue}
  onSubmit={() => console.log('submit')}
  value={value}
  placeholder={'Search'}
/>;
```

# Mobile Search Example with leading, trailing icon and placeholder 

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import Search from '@badger/react/components/Search';
import { Icon, Ripple } from '@badger/react/components/Common';
import SearchIcon from '@zebra/icons/general/icons/search.svg';
import '@zebra/icons';

const [value, setValue] = React.useState('');

<Search
  leading={<SearchIcon width={24} height={24} />}
  onChange={setValue}
  onSubmit={() => console.log('submit')}
  value={value}
  placeholder={'Search'}
  trailing={
    <Ripple onClick={() => console.log('microphone')}>
      <Icon category={'general'} size={24} name={'microphone'} />
    </Ripple>
  }
/>;
```

# Mobile Search Example with trailing icon

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import Search from '@badger/react/components/Search';
import { Ripple } from '@badger/react/components/Common';
import SearchIcon from '@zebra/icons/general/icons/search.svg';

const [value, setValue] = React.useState('');

<Search
  trailing={
    <Ripple onClick={() => console.log('submit')}>
      <SearchIcon width={24} height={24} />
    </Ripple>
  }
  onSubmit={() => console.log('submit')}
  onChange={setValue}
  value={value}
/>;
```

# Mobile Search Example with placeholder and trailing icon

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import Search from '@badger/react/components/Search';
import { Ripple } from '@badger/react/components/Common';
import SearchIcon from '@zebra/icons/general/icons/search.svg';

const [value, setValue] = React.useState(null);

<Search
  trailing={
    <Ripple onClick={() => console.log('submit')}>
      <SearchIcon width={24} height={24} />
    </Ripple>
  }
  onSubmit={() => console.log('submit')}
  placeholder={'Search'}
  onChange={setValue}
  value={value}
/>;
```

# Mobile Search Example with trailing icon prepopulated value and search suggestions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!


```js
import { Menu, MenuItem } from '@badger/react/components/Dropdown';
import { Ripple, Flex } from '@badger/react/components/Common';
import Search from '@badger/react/components/Search';
import SearchIcon from '@zebra/icons/general/icons/search.svg';

const [value, setValue] = React.useState('Pixel 3 phones');
<Flex position={'relative'} flexDirection="column">
  <Search
    trailing={
      <Ripple onClick={() => console.log('submit')}>
        <SearchIcon width={24} height={24} />
      </Ripple>
    }
    onSubmit={() => console.log('submit')}
    placeholder={'Search'}
    onChange={setValue}
    value={value}
  />
  <Menu visible={value && value.length > 3}>
    <MenuItem>{'Show all notification lorem ipsum dolor sit amet'}</MenuItem>
    <MenuItem>{'Hide sensitive lorem ipsum dolor sit amet'}</MenuItem>
    <MenuItem>{'Don’t show notifications at all'}</MenuItem>
  </Menu>
</Flex>;
```

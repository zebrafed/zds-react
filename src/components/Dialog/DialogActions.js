// @flow
import type { ComponentType } from 'react';
import * as React from 'react';
import Flex from '@src/components/Common/Flex';
import styled from 'styled-components';

type ContentProps = {
  children: React.Node,
};

const DialogActions: ComponentType<ContentProps> = styled(Flex).attrs({
  justifyContent: 'flex-end',
  flexDirection: 'column',
  pt: 1,
  pb: 10,
  px: 6,
})`
  ${({ theme }) => theme.lg`
  flex-direction: row;
 `}
`;

export default DialogActions;

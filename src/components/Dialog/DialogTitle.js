// @flow
import * as React from 'react';
import { Component } from 'react';
import type { ComponentType } from 'react';
import Text from '@src/components/Common/Text';
import styled from 'styled-components';
import type { ComponentWithTheme } from '@src/theme';

type ContentProps = {
  children: React.Node,
};

const DialogTitle: ComponentType<ContentProps> = styled(Text).attrs({
  fontSize: 2,
  fontWeight: 1,
  lineHeight: '1.25',
  fontFamily: 'Roboto',
  p: 4,
})`
  display: block;
  ${({ theme }) => theme.lg`
    margin-bottom: 0;
    margin-top: 0;
    padding-top: 0;
  `}
`;

export default DialogTitle;

import { useState } from 'react';

const useDialog = () => {
  const [isVisible, setIsVisible] = useState(false);

  function toggle() {
    setIsVisible(!isVisible);
  }

  return [isVisible, toggle];
};

export default useDialog;

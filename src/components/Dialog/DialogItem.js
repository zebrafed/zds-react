// @flow
import * as React from 'react';
import Flex from '@src/components/Common/Flex';
import styled from 'styled-components';
import Text from '@src/components/Common/Text';
import type { TextProps } from '@src/components/Common/Text';
import type { ComponentType } from 'react';

const reset = `
  margin: 0;
  padding: 0;
`;

export const DialogItemWrap: ComponentType<{ children: React.Node }> = styled(
  Flex
).attrs({
  flex: 1,
  flexDirection: 'row',
  alignContent: 'center',
  px: 4,
  py: 6,
})`
  font-family: 'Roboto';
  border-bottom-width: 1px;
  border-bottom-style: solid;
  border-bottom-color: ${({ theme }) => theme.graySwatches[300]};
  color: ${({ theme }) => theme.graySwatches[800]};
`;

type Props = {
  leading: React.Node,
  children: React.Node,
  onClick?: (e: any) => void,
};

const DialogItem: ComponentType<Props> = ({
  leading,
  children,
  onClick,
}: Props) => {
  return (
    <DialogItemWrap onClick={onClick}>
      {leading && <Flex alignItems="center">{leading}</Flex>}
      <Flex flex="1" flexDirection="column" justifyContent="center" ml={3}>
        {children}
      </Flex>
    </DialogItemWrap>
  );
};
export default DialogItem;

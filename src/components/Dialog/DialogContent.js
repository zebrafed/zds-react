// @flow
import * as React from 'react';
import { Component } from 'react';
import type { ComponentType } from 'react';
import Box from '@src/components/Common/Box';
import styled from 'styled-components';

type ContentProps = {
  children: React.Node,
};

const DialogContent: ComponentType<ContentProps> = styled(Box).attrs({
  fontSize: 1,
  fontWeight: 0,
  lineHeight: '1.25',
  fontFamily: 'Roboto',
  px: 4,
})`
  color: ${({ theme }) => theme.graySwatches[700]};
  ${({ theme }) => theme.lg`
    font-size: ${theme.fontSizes[0]}px;
    line-height: 1.57;
    color: ${theme.graySwatches[800]};
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: normal;
    padding-bottom: 65px;
  `}
`;

export default DialogContent;

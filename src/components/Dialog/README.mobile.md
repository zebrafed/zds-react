# Mobile Dialog Example with two actions, content and title

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  ActionItem,
  useDialog,
} from '@badger/react';

const [isVisible, toggle] = useDialog();
<>
  <button onClick={toggle}>{'Open dialog'}</button>
  <Dialog mobile visible={isVisible} hide={toggle}>
    <DialogTitle as={'h1'}>{'Card title'}</DialogTitle>
    <DialogContent>
      {
        'Message goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam massa quam. Eiusmod tempor incididunt ut labore et dolore magna.'
      }
    </DialogContent>
    <DialogActions>
      <ActionItem onClick={toggle}>{'Ok'}</ActionItem>
      <ActionItem onClick={toggle}>{'Cancel'}</ActionItem>
    </DialogActions>
  </Dialog>
</>;
```

# Mobile Dialog Example with two actions without title

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  ActionItem,
  useDialog,
} from '@badger/react';

const [isVisible, toggle] = useDialog();
<>
  <button onClick={toggle}>{'Open dialog'}</button>
  <Dialog mobile visible={isVisible} hide={toggle}>
    <DialogContent>
      {
        'Message goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam massa quam. Eiusmod tempor incididunt ut labore et dolore magna.'
      }
    </DialogContent>
    <DialogActions>
      <ActionItem onClick={toggle}>{'Ok'}</ActionItem>
      <ActionItem onClick={toggle}>{'Cancel'}</ActionItem>
    </DialogActions>
  </Dialog>
</>;
```

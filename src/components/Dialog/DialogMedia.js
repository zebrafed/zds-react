// @flow
import * as React from 'react';
import { Component } from 'react';
import type { ComponentType } from 'react';
import styled from 'styled-components';
import Box from '@src/components/Common/Box';

type ContentProps = {
  children: React.Node,
};

const DialogMedia: ComponentType<ContentProps> = styled(Box).attrs({
  display: 'block',
  my: 4,
  mx: 0,
})``;

export default DialogMedia;

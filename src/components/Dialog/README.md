# Dialog with Title, Content and two Actions

A central dialog box with a title, body text and two actions that close the dialog. 
​
```js
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  ActionItem,
  useDialog,
} from '@badger/react';
import { Button } from '@zebra-fed/zds-react-desktop' 
import { StripesTheme } from '@zebra-fed/zds-react-desktop/StripesTheme';

const [isVisible, toggle] = useDialog();

<>
  <StripesTheme>
    <Button type={'primary'} onClick={toggle}>{'Open dialog'}</Button>
  </StripesTheme>
  <Dialog visible={isVisible} hide={toggle} mobile={true}>
    <DialogTitle as={'h1'}>{'Title that may take two lines:'}</DialogTitle>
    <DialogContent>
      {
        'Secondary text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam massa quam. Eiusmod tempor incididunt ut labore.'
      }
    </DialogContent>
    <DialogActions>
      <ActionItem onClick={toggle}>{'Ok'}</ActionItem>
      <ActionItem onClick={toggle}>{'Cancel'}</ActionItem>
    </DialogActions>
  </Dialog>
</>;
```

# Dialog with Title, Content and three Actions

A central dialog box with a title, body text and three actions that close the dialog. 

```js
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  ActionItem,
  useDialog,
} from '@badger/react';
import { Button } from '@zebra-fed/zds-react-desktop' 
import { StripesTheme } from '@zebra-fed/zds-react-desktop/StripesTheme';

const [isVisible, toggle] = useDialog();

<>
  <StripesTheme>
    <Button type={'primary'} onClick={toggle}>{'Open dialog'}</Button>
  </StripesTheme>
  <Dialog visible={isVisible} hide={toggle}>
    <DialogTitle as={'h1'}>{'Title that may take two lines:'}</DialogTitle>
    <DialogContent>
      {
        'Secondary text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam massa quam. Eiusmod tempor incididunt ut labore.'
      }
    </DialogContent>
    <DialogActions>
      <ActionItem onClick={toggle}>{'Ok'}</ActionItem>
      <ActionItem onClick={toggle}>{'Cancel'}</ActionItem>
      <ActionItem onClick={toggle}>{'Default'}</ActionItem>
    </DialogActions>
  </Dialog>
</>;
```

# Dialog with Title, Content, Media and Actions

A central dialog box with a title, body text, a placeholder image, example position of an audio staging environment and an action.

```js
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogMedia,
  ActionItem,
  useDialog,
  Text,
} from '@badger/react';
import { ThemeContext } from 'styled-components';
import { Button } from '@zebra-fed/zds-react-desktop' 
import { StripesTheme } from '@zebra-fed/zds-react-desktop/StripesTheme';

const theme = React.useContext(ThemeContext);
const [isVisible, toggle] = useDialog();

<>
  <StripesTheme>
    <Button type={'primary'} onClick={toggle}>{'Open dialog'}</Button>
  </StripesTheme>
  <Dialog visible={isVisible} hide={toggle}>
    <DialogTitle as={'h1'}>{'Audio Staging help'}</DialogTitle>
    <DialogContent>
      {
        'Audio staging is a new beta feature in StageNow tha allows you to stage multiple devices at once, using a single audio file.'
      }
    </DialogContent>
    <DialogMedia as={'img'} src={'https://via.placeholder.com/208x104'} />
    <Text p={4} color={theme.graySwatches[700]} fontSize={12}>
      {'Ideal audio staging environment'}
    </Text>
    <DialogActions>
      <ActionItem onClick={toggle}>{'Dismiss'}</ActionItem>
    </DialogActions>
  </Dialog>
</>;
```

# Bottom Dialog Example

A bottom dialog with two example menu items and two actions.

```js
import {
  Dialog,
  DialogTitle,
  DialogActions,
  DialogItem,
  ActionItem,
  useDialog,
} from '@badger/react';
import { ThemeContext } from 'styled-components';
import KnowledgeArticles from '@zebra/icons/content/icons/knowledge-articles.svg';
import Folder from '@zebra/icons/general/icons/folder.svg';
import { Button } from '@zebra-fed/zds-react-desktop' 
import { StripesTheme } from '@zebra-fed/zds-react-desktop/StripesTheme';

const theme = React.useContext(ThemeContext);

const [isVisible, toggle] = useDialog();

<>
  <StripesTheme>
    <Button type={'primary'} onClick={toggle}>{'Open dialog'}</Button>
  </StripesTheme>
  <Dialog visible={isVisible} hide={toggle} bottom>
    <DialogTitle as={'h1'}>{'Audio Staging help'}</DialogTitle>
    <DialogItem
      leading={<KnowledgeArticles width={24} fill={theme.primaryColor} />}
      onClick={(e) => console.log(e)}
    >
      {'Enterprise Home Screen'}
    </DialogItem>
    <DialogItem
      leading={<Folder width={24} fill={theme.primaryColor} />}
      onClick={(e) => console.log(e)}
    >
      {'Launcher 3'}
    </DialogItem>
    <DialogActions>
      <ActionItem onClick={toggle}>{'Just once'}</ActionItem>
      <ActionItem onClick={toggle}>{'Aways'}</ActionItem>
    </DialogActions>
  </Dialog>
</>;
```

// @flow
import type { ComponentType } from 'react';
import * as React from 'react';
import { useState } from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import type { ComponentWithTheme } from '@src/theme';
import Color from 'color';
import Box from '@src/components/Common/Box';
import Text from '@src/components/Common/Text';

type Props = {
  /**Contains all the dialogs sub-components; DialogTitle, DialogContent, etc. */
  children?: React.Node,
  /**A boolean that sets the visibility of the dialog. It's recommended that this is set using the useDialog hook (see examples).*/
  visible?: boolean,
  /**Snaps the dialog along the bottom of the page.*/
  bottom?: boolean,
  mobile?: boolean,
};

const DialogOverlay = styled(Box).attrs({})`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: #000;
  opacity: 0.8;
  z-index: 200;
`;

const DialogStyles = styled(Box).attrs({
  p: 0,
  m: 0,
  maxWidth: 208,
  flexDirection: 'column',
  justifyContent: 'space-between',
})`
  position: fixed;
  box-sizing: border-box;
  left: ${({ bottom }) => (bottom ? 'auto' : '50%')};
  top: ${({ bottom }) => (bottom ? 'auto' : '50%')};
  bottom: ${({ bottom }) => (bottom ? '0' : 'auto')};
  max-width: ${({ bottom }) => (bottom ? '100%' : 'auto')};
  width: ${({ bottom }) => (bottom ? '100%' : 'auto')};
  transform: ${({ bottom }) =>
    bottom ? 'translate(0, 0)' : 'translate(-50%, -50%)'};
  background-color: ${({ theme }) => theme.white};
  z-index: 201;
  ${({ theme, bottom }) => theme.lg`
    max-width: ${bottom ? '100%' : '320px'};
    padding-left: 8px;
    padding-right: 8px;
    padding-top: 18px;
  `}
`;

/**
 * Dialogs inform users about a task and can contian critical information, require decisions, or involve multiple tasks.
 *
 * @example import { Dialog } from '@badger/react';
 */
const Dialog: ComponentType<Props> = ({
  visible = false,
  bottom = false,
  children,
  mobile = false,
}: Props) => {
  const body = document.body;

  if (!body) {
    return <></>;
  }

  return visible
    ? ReactDOM.createPortal(
      <>
        <DialogOverlay />
        <DialogStyles
          aria-modal
          aria-hidden
          tabIndex={-1}
          role="dialog"
          bottom={bottom}
          mobile={mobile}
        >
          {children}
        </DialogStyles>
      </>,
      body
    )
    : null;
};

export default Dialog;

// @flow
import type { ComponentType } from 'react';
import * as React from 'react';
import Flex from '@src/components/Common/Flex';
import styled from 'styled-components';

type ContentProps = {
  children: React.Node,
};

const CardActions: ComponentType<ContentProps> = styled(Flex).attrs({
  justifyContent: 'flex-end',
  pt: 1,
  pb: 10,
  px: 4,
})`
  ${({ theme }) => theme.lg`
    padding-top: 14px;
    padding-bottom: 17px;
  `}
`;

export default CardActions;

// @flow
import type { ComponentType } from 'react';
import * as React from 'react';
import styled from 'styled-components';
import type { ComponentWithTheme } from '@src/theme';
import Color from 'color';
import Box from '@src/components/Common/Box';


type Props = {
  /** Contains all the Card's sub-components; CardHeader, CardContent, etc.*/
  children?: React.Node,
};

const CardContent: ComponentType<Props> = styled(Box).attrs({
  px: 0,
  my: 6,
  mx: 4,
  flexDirection: 'column',
  justifyContent: 'space-between',
})`
  background-color: ${({ theme }) => theme.white};
  ${({ theme }) => theme.lg`
  border-bottom: 1px solid ${theme.graySwatches[300]}
  `}
`;

/**
 * A Card displays related content and actions in a single area.
 * @example import { Card } from '@badger/react';
 */
const Card = ({ children }: Props) => <CardContent>{children}</CardContent>;

export default Card;

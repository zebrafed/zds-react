# Default Card Example

A card with a title and subheader with a leading icon and body text.

```js
import { Card, Box, CardHeader, CardContent, Icon } from '@badger/react';
import { ThemeContext } from 'styled-components';
import Pin from '@zebra/icons/transportationand-logistics/icons/pin.svg';
import '@zebra/icons/transportationand-logistics/index.css';

const theme = React.useContext(ThemeContext);

<Card>
  <CardHeader
    leading={
      <Icon
        size={24}
        color={theme.primaryColor}
        category={'transportationand-logistics'}
        name={'pin'}
      />
    }
    title={'List item'}
    subheader={'Details'}
  />
  <CardContent>
    {
      'Secondary text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam massa quam.'
    }
  </CardContent>
</Card>;
```

# Card with actions

A card with two actions.

```js
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  ActionItem,
} from '@badger/react';
import { ThemeContext } from 'styled-components';
import Pin from '@zebra/icons/transportationand-logistics/icons/pin.svg';

const theme = React.useContext(ThemeContext);

<Card>
  <CardHeader
    leading={<Pin width={24} height={24} fill={theme.primaryColor} />}
    title={'List item'}
    subheader={'Details'}
  />
  <CardContent>
    {
      'Secondary text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam massa quam.'
    }
  </CardContent>
  <CardActions>
    <ActionItem onClick={(e) => console.log(e)}>{'Action 1'}</ActionItem>
    <ActionItem onClick={(e) => console.log(e)}>{'Action 2'}</ActionItem>
  </CardActions>
</Card>;
```

# Card with media

A card displaying a 224x210 image.

```js
import { Card, CardHeader, CardMedia } from '@badger/react';
import { ThemeContext } from 'styled-components';
import Pin from '@zebra/icons/transportationand-logistics/icons/pin.svg';

const theme = React.useContext(ThemeContext);

<Card>
  <CardHeader
    leading={<Pin width={24} height={24} fill={theme.primaryColor} />}
    title={'List item'}
    subheader={'Details'}
  />
  <CardMedia as={'img'} src={'https://via.placeholder.com/224x210'} />
</Card>;
```

# Card with media and actions

A card displaying an image with two basic actions.

```js
import {
  Card,
  CardHeader,
  CardMedia,
  CardActions,
  ActionItem,
} from '@badger/react';
import { ThemeContext } from 'styled-components';
import Pin from '@zebra/icons/transportationand-logistics/icons/pin.svg';

const theme = React.useContext(ThemeContext);

<Card>
  <CardHeader
    leading={<Pin width={24} height={24} fill={theme.primaryColor} />}
    title={'List item'}
    subheader={'Details'}
  />
  <CardMedia as={'img'} src={'https://via.placeholder.com/224x97'} />
  <CardActions>
    <ActionItem onClick={(e) => console.log(e)}>{'Action 1'}</ActionItem>
    <ActionItem onClick={(e) => console.log(e)}>{'Action 2'}</ActionItem>
  </CardActions>
</Card>;
```

# Card with different content and actions

A card with a title, body text, an image and two actions.

```js
import {
  Card,
  CardContent,
  CardActions,
  ActionItem,
  Text,
  Box,
} from '@badger/react';

<Card>
  <CardContent>
    <>
      <Text as={Box} lineHeight={2} fontWeight={1} fontSize={1}>
        {'End detection timeout (sec):'}
      </Text>
      <Text>
        {
          'Zero is a special value. If “End phrase” is defined, it is infinite, else, data is returned immediately.'
        }
      </Text>
      <Box
        as={'img'}
        mt={5}
        width={1}
        src={'https://via.placeholder.com/212x48'}
      />
    </>
  </CardContent>
  <CardActions>
    <ActionItem onClick={(e) => console.log(e)}>{'Cancel'}</ActionItem>
    <ActionItem onClick={(e) => console.log(e)}>{'Ok'}</ActionItem>
  </CardActions>
</Card>;
```

# Swipeable card example with one action

A card with main content and a swipe-to-reveal action.

```js
import {
  CardHeader,
  Swipeable,
  ActionItem,
  Text,
  Box,
  Flex,
} from '@badger/react';
import styled, { ThemeContext } from 'styled-components';
import Pin from '@zebra/icons/transportationand-logistics/icons/pin.svg';

const theme = React.useContext(ThemeContext);

const Circle = styled(Box)`
  width: 12px;
  height: 12px;
  margin-right: 5px;
  border-radius: 50%;
  background-color: ${({ theme }) => theme.secondaryColors[0]};
`;

const Placeholder = styled(Box).attrs({
  width: 48,
  height: 48,
})`
  background-color: ${({ theme }) => theme.primaryColor};
`;

<Swipeable
  actions={
    <ActionItem color={theme.primaryColor} onClick={(e) => console.log(e)}>
      <Text
        fontSize={12}
        fontWeight={500}
        style={{
          textTransform: 'none',
        }}
      >
        {'Test audio'}
      </Text>
    </ActionItem>
  }
>
  <CardHeader
    leading={<Placeholder />}
    title={
      <Flex justifyContent={'space-between'}>
        <Flex alignItems={'center'}>
          <Circle />
          <Text fontWeight={2}>{'HS3100'}</Text>
        </Flex>
        <Text fontSize={12} color={theme.secondaryColors[0]}>
          {'14:47'}
        </Text>
      </Flex>
    }
    subheader={
      <Flex flexDirection={'column'}>
        <Text fontWeight={1} fontSize={12}>
          {'Battery: 95%'}
        </Text>
        <Text fontWeight={1} fontSize={12}>
          {'Health: Normal'}
        </Text>
      </Flex>
    }
  />
</Swipeable>;
```

# Swipeable card example with multiple actions

A card with main content and two swipe-to-reveal actions.

```js
import {
  CardHeader,
  Swipeable,
  ActionItem,
  Text,
  Box,
  Flex,
} from '@badger/react';
import styled, { ThemeContext } from 'styled-components';
import Pin from '@zebra/icons/transportationand-logistics/icons/pin.svg';

const theme = React.useContext(ThemeContext);

const Circle = styled(Box)`
  width: 12px;
  height: 12px;
  margin-right: 5px;
  border-radius: 50%;
  background-color: ${({ theme }) => theme.secondaryColors[0]};
`;

const Placeholder = styled(Box).attrs({
  width: 48,
  height: 48,
})`
  background-color: ${({ theme }) => theme.primaryColor};
`;

<Swipeable
  actions={
    <>
      <ActionItem color={theme.dangerColor} onClick={(e) => console.log(e)}>
        <Text
          fontSize={12}
          fontWeight={500}
          style={{
            textTransform: 'none',
          }}
        >
          {'Test audio 2'}
        </Text>
      </ActionItem>
      <ActionItem color={theme.primaryColor} onClick={(e) => console.log(e)}>
        <Text
          fontSize={12}
          fontWeight={500}
          style={{
            textTransform: 'none',
          }}
        >
          {'Test audio 1'}
        </Text>
      </ActionItem>
    </>
  }
>
  <CardHeader
    leading={<Placeholder />}
    title={
      <Flex justifyContent={'space-between'}>
        <Flex alignItems={'center'}>
          <Circle />
          <Text fontWeight={2}>{'HS3100'}</Text>
        </Flex>
        <Text fontSize={12} color={theme.secondaryColors[0]}>
          {'14:47'}
        </Text>
      </Flex>
    }
    subheader={
      <Flex flexDirection={'column'}>
        <Text fontWeight={1} fontSize={12}>
          {'Battery: 95%'}
        </Text>
        <Text fontWeight={1} fontSize={12}>
          {'Health: Normal'}
        </Text>
      </Flex>
    }
  />
</Swipeable>;
```

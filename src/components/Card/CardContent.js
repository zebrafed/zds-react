// @flow
import * as React from 'react';
import { Component } from 'react';
import type { ComponentType } from 'react';
import styled from 'styled-components';
import type { ComponentWithTheme } from '@src/theme';
import Box from '@src/components/Common/Box';

type ContentProps = {
  children: React.Node,
};

const CardContent: ComponentType<ContentProps> = styled(Box).attrs({
  fontSize: 0,
  lineHeight: '1.29',
  fontFamily: 'Roboto',
  p: 4,
})``;

export default CardContent;

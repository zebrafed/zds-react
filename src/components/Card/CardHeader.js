// @flow
import * as React from 'react';
import Flex from '@src/components/Common/Flex';
import styled from 'styled-components';
import Text from '@src/components/Common/Text';
import type { TextProps } from '@src/components/Common/Text';
import type { ComponentType } from 'react';

const reset = `
  margin: 0;
  padding: 0;
`;

const borderBottom = ({ size = 1, color }: { size: number, color: string }) => `
  border-bottom-width: ${size}px;
  border-bottom-style: solid;
  border-bottom-color: ${color};
`;

export const Heading: ComponentType<TextProps> = styled(Text)`
  ${reset}
`;

export const SubHeading: ComponentType<TextProps> = styled(Text)`
  ${reset}
  color: ${({ theme }) => theme.graySwatches[500]};
  ${({ theme }) => theme.lg`
    line-height: 1.43;
    margin-top: 1px;
  `}
`;

export const CardHeaderWrap: ComponentType<{ children: React.Node }> = styled(
  Flex
).attrs({
  flex: 1,
  flexDirection: 'row',
  alignContent: 'center',
  px: 4,
  py: 6,
})`
  margin: 0;
  box-sizing: border-box;
  min-height: 56px;
  ${({ theme }) => borderBottom({ size: 1, color: theme.graySwatches[300] })}
  ${({ theme }) => theme.lg`
    padding-top: ${theme.space[10]}px;
    border-bottom: none;
  `}
`;

export const CardHeaderIcon: ComponentType<{ children: React.Node }> = styled(
  Flex
)`
  ${({ theme }) => theme.lg`
    align-items: unset;
  `}
`;

type Props = {
  leading: React.Node,
  title: String,
  subheader: String,
  trailing: React.Node,
};

const CardHeader: ComponentType<Props> = ({
  leading,
  title,
  subheader,
  trailing,
}: Props) => {
  return (
    <CardHeaderWrap>
      {leading && (
        <CardHeaderIcon alignItems="center">
          {leading}
        </CardHeaderIcon>
      )}
      <Flex flex="1" flexDirection="column" justifyContent="center" ml={3}>
        <Heading as={'h1'} fontSize={1} lineHeight={1} fontWeight={0}>
          {title}
        </Heading>
        {subheader && (
          <SubHeading as={'h2'} fontSize={0} lineHeight={1.14} fontWeight={0}>
            {subheader}
          </SubHeading>
        )}
      </Flex>
      {trailing && (
        <CardHeaderIcon alignItems="center">
          {trailing}
        </CardHeaderIcon>
      )}
    </CardHeaderWrap>
  );
};
export default CardHeader;

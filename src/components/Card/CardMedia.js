// @flow
import * as React from 'react';
import { Component } from 'react';
import type { ComponentType } from 'react';
import Flex from '@src/components/Common/Flex';
import Text from '@src/components/Common/Text';
import styled from 'styled-components';
import type { ComponentWithTheme } from '@src/theme';
import { space, typography } from 'styled-system';
import Box from '@src/components/Common/Box';

type ContentProps = {
  children: React.Node,
};

const CardMedia: ComponentType<ContentProps> = styled(Box).attrs({
  width: 1,
  display: 'block',
})``;

export default CardMedia;

# Mobile Card Example with tertiary information

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import {
  Card,
  Box,
  CardHeader,
  CardContent,
  Icon,
  Flex,
  Text,
} from '@badger/react';
import { ThemeContext } from 'styled-components';
import '@zebra/icons/general/index.css';

const theme = React.useContext(ThemeContext);

<Card>
  <CardHeader
    leading={
      <Icon
        size={32}
        color={theme.primaryColor}
        category={'general'}
        name={'person'}
      />
    }
    title={'Simple card'}
    subheader={
      <Flex flexDirection="column">
        <Text paddingTop={1}>Secondary information</Text>
        <Text fontSize={12} paddingTop={3} paddingBottom={3}>
          Tertiary information
        </Text>
      </Flex>
    }
    trailing={
      <Icon
        size={24}
        color={theme.primaryColor}
        category={'general'}
        name={'clock'}
      />
    }
  />
</Card>;
```

# Mobile Card Example with actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import {
  Card,
  Box,
  CardHeader,
  CardContent,
  CardActions,
  ActionItem,
  Icon,
  Flex,
  Text,
} from '@badger/react';
import { ThemeContext } from 'styled-components';
import Pin from '@zebra/icons/transportationand-logistics/icons/pin.svg';
import '@zebra/icons/transportationand-logistics/index.css';

const theme = React.useContext(ThemeContext);

<Card>
  <CardHeader
    leading={
      <Icon
        size={32}
        color={theme.primaryColor}
        category={'general'}
        name={'person'}
      />
    }
    title={'Simple card'}
    subheader={'Secondary information'}
    trailing={
      <Icon
        size={24}
        color={theme.primaryColor}
        category={'general'}
        name={'clock'}
      />
    }
  />
  <CardActions>
    <ActionItem onClick={(e) => console.log(e)}>{'Action 1'}</ActionItem>
    <ActionItem onClick={(e) => console.log(e)}>{'Action 2'}</ActionItem>
  </CardActions>
</Card>;
```

# Mobile Card Example, tertiary information and actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import {
  Card,
  Box,
  CardHeader,
  CardContent,
  CardActions,
  ActionItem,
  Icon,
  Flex,
  Text,
} from '@badger/react';
import { ThemeContext } from 'styled-components';
import Pin from '@zebra/icons/transportationand-logistics/icons/pin.svg';
import '@zebra/icons/transportationand-logistics/index.css';

const theme = React.useContext(ThemeContext);

<Card>
  <CardHeader
    leading={
      <Icon
        size={32}
        color={theme.primaryColor}
        category={'general'}
        name={'person'}
      />
    }
    title={'Simple card'}
    subheader={
      <Flex flexDirection="column">
        <Text paddingTop={1}>Secondary information</Text>
        <Text fontSize={12} paddingTop={3} paddingBottom={3}>
          Tertiary information
        </Text>
      </Flex>
    }
    trailing={
      <Icon
        size={24}
        color={theme.primaryColor}
        category={'general'}
        name={'clock'}
      />
    }
  />
  <CardActions>
    <ActionItem onClick={(e) => console.log(e)}>{'Action 1'}</ActionItem>
    <ActionItem onClick={(e) => console.log(e)}>{'Action 2'}</ActionItem>
  </CardActions>
</Card>;
```

# Mobile Card Example with media

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import { Card, CardHeader, CardMedia, Icon } from '@badger/react';
import { ThemeContext } from 'styled-components';
import Pin from '@zebra/icons/general/icons/person.svg';

const theme = React.useContext(ThemeContext);

<Card>
  <CardHeader
    leading={<Pin width={32} height={32} fill={theme.primaryColor} />}
    trailing={
      <Icon
        size={24}
        color={theme.primaryColor}
        category={'general'}
        name={'clock'}
      />
    }
    title={'Simple card'}
    subheader={'Secondary information'}
  />
  <CardMedia as={'img'} src={'https://via.placeholder.com/344x160'} />
</Card>;
```

# Mobile Card Example with media and actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import {
  Card,
  CardHeader,
  CardMedia,
  Icon,
  CardActions,
  ActionItem,
} from '@badger/react';
import { ThemeContext } from 'styled-components';
import Pin from '@zebra/icons/general/icons/person.svg';

const theme = React.useContext(ThemeContext);

<Card>
  <CardHeader
    leading={<Pin width={32} height={32} fill={theme.primaryColor} />}
    trailing={
      <Icon
        size={24}
        color={theme.primaryColor}
        category={'general'}
        name={'clock'}
      />
    }
    title={'Simple card'}
    subheader={'Secondary information'}
  />
  <CardMedia as={'img'} src={'https://via.placeholder.com/344x160'} />
  <CardActions>
    <ActionItem onClick={(e) => console.log(e)}>{'Action 1'}</ActionItem>
    <ActionItem onClick={(e) => console.log(e)}>{'Action 2'}</ActionItem>
  </CardActions>
</Card>;
```

# Mobile Card Example, tertiary info with media

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import {
  Card,
  CardHeader,
  CardMedia,
  Icon,
  CardActions,
  ActionItem,
  Flex,
  Text,
} from '@badger/react';
import { ThemeContext } from 'styled-components';
import Pin from '@zebra/icons/general/icons/person.svg';

const theme = React.useContext(ThemeContext);

<Card>
  <CardHeader
    leading={<Pin width={32} height={32} fill={theme.primaryColor} />}
    trailing={
      <Icon
        size={24}
        color={theme.primaryColor}
        category={'general'}
        name={'clock'}
      />
    }
    title={'Simple card'}
    subheader={
      <Flex flexDirection="column">
        <Text paddingTop={1}>Secondary information</Text>
        <Text fontSize={12} paddingTop={3} paddingBottom={3}>
          Tertiary information
        </Text>
      </Flex>
    }
  />
  <CardMedia as={'img'} src={'https://via.placeholder.com/344x160'} />
</Card>;
```

# Mobile Card Example, tertiary info with media and actions

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import {
  Card,
  CardHeader,
  CardMedia,
  Icon,
  CardActions,
  ActionItem,
  Flex,
  Text,
} from '@badger/react';
import { ThemeContext } from 'styled-components';
import Person from '@zebra/icons/general/icons/person.svg';

const theme = React.useContext(ThemeContext);

<Card>
  <CardHeader
    leading={<Person width={32} height={32} fill={theme.primaryColor} />}
    trailing={
      <Icon
        size={24}
        color={theme.primaryColor}
        category={'general'}
        name={'clock'}
      />
    }
    title={'Simple card'}
    subheader={
      <Flex flexDirection="column">
        <Text paddingTop={1}>Secondary information</Text>
        <Text fontSize={12} paddingTop={3} paddingBottom={3}>
          Tertiary information
        </Text>
      </Flex>
    }
  />
  <CardMedia as={'img'} src={'https://via.placeholder.com/344x160'} />
  <CardActions>
    <ActionItem onClick={(e) => console.log(e)}>{'Action 1'}</ActionItem>
    <ActionItem onClick={(e) => console.log(e)}>{'Action 2'}</ActionItem>
  </CardActions>
</Card>;
```

# Mobile Swipeable card example with one action

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import {
  CardHeader,
  Swipeable,
  ActionItem,
  Text,
  Box,
  Flex,
  Icon,
} from '@badger/react';
import styled, { ThemeContext } from 'styled-components';
import Person from '@zebra/icons/general/icons/person.svg';
import Delete from '@zebra/icons/general/icons/delete.svg';

const theme = React.useContext(ThemeContext);

<Swipeable
  actions={
    <ActionItem color={theme.primaryColor} onClick={(e) => console.log(e)}>
      <Delete width={24} height={32} fill={theme.white} />
    </ActionItem>
  }
>
  <CardHeader
    leading={<Person width={32} height={32} fill={theme.primaryColor} />}
    trailing={
      <Icon
        size={24}
        color={theme.primaryColor}
        category={'general'}
        name={'clock'}
      />
    }
    title={'Swipe card'}
    subheader={
      <Flex flexDirection="column">
        <Text paddingTop={1}>Secondary information</Text>
        <Text fontSize={12} paddingTop={3} paddingBottom={3}>
          Tertiary information
        </Text>
      </Flex>
    }
  />
</Swipeable>;
```

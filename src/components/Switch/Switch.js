// @flow
import React, { useState, useCallback, useContext } from 'react';
import type { ComponentType } from 'react';
import styled, { css, ThemeContext } from 'styled-components';
import Box from '@src/components/Common/Box';
import Flex from '@src/components/Common/Flex';
import { HiddenCheckbox } from '@src/components/Checkbox/Checkbox';
import Ripple from '@src/components/Common/Ripple';
import type { OnlyChildren } from '@src/components/Common/Swipeable';

const SwitchRail: ComponentType<{}> = styled(Box).attrs({
  width: 36,
  height: 16,
})`
  border-radius: 10000px;
  background-color: ${({ theme }) => theme.graySwatches[300]};
`;

const SwitchTumb: ComponentType<{}> = styled(Box).attrs({
  width: 24,
  height: 24,
})`
  box-sizing: border-box;
  border: 4px solid ${({ theme }) => theme.black};
  border-radius: 100000px;
  background: ${({ theme }) => theme.white};
`;

const SwitchWrapper: ComponentType<OnlyChildren> = styled(Flex).attrs({
  alignItems: 'center',
  width: 36,
  height: 36,
})`
  position: relative;

  ${HiddenCheckbox}:checked ~ ${SwitchTumb} {
    transform: translateX(12px);
  }

  ${HiddenCheckbox}:checked ~ ${SwitchRail} {
    background: ${({ theme }) => theme.primaryColor};
  }

  ${HiddenCheckbox}:disabled ~ ${SwitchTumb} {
    border-color: ${({ theme }) => theme.graySwatches[500]};
    background-color: ${({ theme }) => theme.graySwatches[500]};
  }
  ${HiddenCheckbox}:disabled ~ ${SwitchRail} {
    background-color: ${({ theme }) => theme.graySwatches[300]};
  }

  ${SwitchRail} {
    position: absolute;
    transition: all 0.1s linear;
  }

  ${SwitchTumb} {
    position: absolute;
    transform: translateX(0);
    transition: all 0.1s linear;
  }
`;

type Props = {
  /**A boolean that states the switch's initial checked state.*/
  checked?: boolean,
  /**A callback function that is fired whenever the switch is toggled. */
  onChange?: (checked: boolean, value: any) => void,
  /**A boolean that disables the switch. */
  disabled?: boolean,
  /**The value assigned to the switch for use with forms. */
  value: string,
};

/**
 * Switches are used to toggle a single item on or off.
 *
 * @example import { Switch } from '@badger/react';
 */
const Switch: ComponentType<Props> = ({
  disabled,
  value,
  onChange,
  ...props
}: Props) => {
  const [checked, setChecked] = useState(props.checked);
  const handleChange = useCallback(() => {
    const newChecked = !checked;
    setChecked(newChecked);
    if (onChange) {
      onChange(newChecked, value);
    }
  }, [checked]);

  const theme = useContext(ThemeContext);

  return (
    <Ripple
      disable={disabled}
      color={checked ? theme.primaryColor : theme.graySwatches[100]}
    >
      <SwitchWrapper>
        <HiddenCheckbox
          disabled={disabled}
          checked={checked}
          value={value}
          onChange={handleChange}
        />
        <SwitchRail />
        <SwitchTumb />
      </SwitchWrapper>
    </Ripple>
  );
};

export default Switch;

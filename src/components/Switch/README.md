# Switch examples left aligned

Examples of all the possible states switches can be in.

```jsx
import { FormControl, Switch, Box } from '@badger/react';

<Box px={4} py={6}>
  <FormControl
    reverse
    control={
      <Switch
        checked={false}
        onChange={(checked, value) => console.log(checked, value)}
        value={'unchecked'}
      />
    }
  >
    {'Switch off'}
  </FormControl>
  <FormControl
    reverse
    control={
      <Switch
        checked={true}
        onChange={(checked, value) => console.log(checked, value)}
        value={'selected'}
      />
    }
  >
    {'Switch on'}
  </FormControl>
  <FormControl
    reverse
    control={
      <Switch
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Switch Disabled'}
  </FormControl>
  <FormControl
    reverse
    control={
      <Switch
        checked={true}
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Switch on Disabled'}
  </FormControl>
</Box>;
```

# Switch examples right aligned

A group of right-aligned switches.

```jsx
import { FormControl, Switch, Box } from '@badger/react';

<Box px={4} py={6}>
  <FormControl
    control={
      <Switch
        checked={false}
        onChange={(checked, value) => console.log(checked, value)}
        value={'unchecked'}
      />
    }
  >
    {'Switch off'}
  </FormControl>
  <FormControl
    control={
      <Switch
        checked={true}
        onChange={(checked, value) => console.log(checked, value)}
        value={'selected'}
      />
    }
  >
    {'Switch on'}
  </FormControl>
  <FormControl
    control={
      <Switch
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Switch Disabled'}
  </FormControl>
  <FormControl
    control={
      <Switch
        checked={true}
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Switch on Disabled'}
  </FormControl>
</Box>;
```

# Switch examples more content

Switches with content that spans multiple lines.

```jsx
import { FormControl, Switch, Box } from '@badger/react';

<Box px={4} py={6}>
  <FormControl
    control={
      <Switch
        checked={true}
        onChange={(checked, value) => console.log(checked, value)}
        value={'item'}
      />
    }
  >
    {'Item'}
  </FormControl>
  <FormControl
    description={'Details'}
    control={
      <Switch
        checked={true}
        onChange={(checked, value) => console.log(checked, value)}
        value={'item'}
      />
    }
  >
    {'Item'}
  </FormControl>
  <FormControl
    description={'Details, in case we need a two line long explanation'}
    control={
      <Switch
        checked={true}
        onChange={(checked, value) => console.log(checked, value)}
        value={'item'}
      />
    }
  >
    {'Item'}
  </FormControl>
  <FormControl
    description={'Details, in case we need a two line long explanation'}
    control={
      <Switch
        checked={true}
        onChange={(checked, value) => console.log(checked, value)}
        value={'item'}
      />
    }
  >
    {'Item with a long title that has two lines'}
  </FormControl>
</Box>;
```

# Default Radio Example

All possible states of a radio button.

```jsx
import { FormControl, Radio, Box } from '@badger/react';
<Box px={4} py={6}>
  <FormControl
    control={
      <Radio
        checked={false}
        onChange={(checked, value) => console.log(checked, value)}
        value={'unchecked'}
      />
    }
  >
    {'Radio unchecked'}
  </FormControl>
  <FormControl
    control={
      <Radio
        checked={true}
        onChange={(checked, value) => console.log(checked, value)}
        value={'selected'}
      />
    }
  >
    {'Radio selected'}
  </FormControl>
  <FormControl
    control={
      <Radio
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Radio disabled'}
  </FormControl>
  <FormControl
    control={
      <Radio
        checked={true}
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Radio disabled'}
  </FormControl>
</Box>;
```

# Grouped Radio Example

A group of radio buttons allowing only one option to be selected.

```jsx
import { FormControl, Radio, Box } from '@badger/react';
import useRadioGroup from '../Common/UseRadioGroupHook';
const [checked, setChecked] = useRadioGroup(1);
<Box px={4} py={6}>
  <FormControl
    control={
      <Radio
        key={1}
        name={'gender'}
        checked={!!checked[1]}
        onChange={(checked, value) => {
          console.log(checked, value);
          setChecked(1);
        }}
        value={'f'}
      />
    }
  >
    {'Female'}
  </FormControl>
  <FormControl
    control={
      <Radio
        key={2}
        name={'gender'}
        checked={!!checked[2]}
        onChange={(checked, value) => {
          console.log(checked, value);
          setChecked(2);
        }}
        value={'m'}
      />
    }
  >
    {'Male'}
  </FormControl>
  <FormControl
    control={
      <Radio
        key={3}
        name={'gender'}
        checked={!!checked[3]}
        onChange={(checked, value) => {
          console.log(checked, value);
          setChecked(3);
        }}
        value={'o'}
      />
    }
  >
    {'Other'}
  </FormControl>
</Box>;
```

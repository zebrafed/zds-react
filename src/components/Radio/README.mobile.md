# Mobile Radio Example

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```jsx
import { FormControl, Radio, Box } from '@badger/react';
<Box px={4} py={6}>
  <FormControl
    control={
      <Radio
        checked={false}
        onChange={(checked, value) => console.log(checked, value)}
        value={'unchecked'}
      />
    }
  >
    {'Radio unchecked'}
  </FormControl>
  <FormControl
    control={
      <Radio
        checked={true}
        onChange={(checked, value) => console.log(checked, value)}
        value={'selected'}
      />
    }
  >
    {'Radio selected'}
  </FormControl>
  <FormControl
    control={
      <Radio
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Radio disabled'}
  </FormControl>
  <FormControl
    control={
      <Radio
        checked={true}
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Radio disabled'}
  </FormControl>
</Box>;
```

# Mobile Radio Example without border

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```jsx
import { FormControl, Radio, Box } from '@badger/react';
<Box px={4} py={6}>
  <FormControl
    noborder
    control={
      <Radio
        checked={false}
        onChange={(checked, value) => console.log(checked, value)}
        value={'unchecked'}
      />
    }
  >
    {'Radio unchecked'}
  </FormControl>
  <FormControl
    noborder
    control={
      <Radio
        checked={true}
        onChange={(checked, value) => console.log(checked, value)}
        value={'selected'}
      />
    }
  >
    {'Radio selected'}
  </FormControl>
  <FormControl
    noborder
    control={
      <Radio
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Radio disabled'}
  </FormControl>
  <FormControl
    noborder
    control={
      <Radio
        checked={true}
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Radio disabled'}
  </FormControl>
</Box>;
```

# Mobile grouped Radio Example

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```jsx
import { FormControl, Radio, Box } from '@badger/react';
import useRadioGroup from '../Common/UseRadioGroupHook';
const [checked, setChecked] = useRadioGroup(1);
<Box px={4} py={6}>
  <FormControl
    control={
      <Radio
        key={1}
        name={'gender'}
        checked={!!checked[1]}
        onChange={(checked, value) => {
          console.log(checked, value);
          setChecked(1);
        }}
        value={'f'}
      />
    }
  >
    {'Female'}
  </FormControl>
  <FormControl
    control={
      <Radio
        key={2}
        name={'gender'}
        checked={!!checked[2]}
        onChange={(checked, value) => {
          console.log(checked, value);
          setChecked(2);
        }}
        value={'m'}
      />
    }
  >
    {'Male'}
  </FormControl>
  <FormControl
    control={
      <Radio
        key={3}
        name={'gender'}
        checked={!!checked[3]}
        onChange={(checked, value) => {
          console.log(checked, value);
          setChecked(3);
        }}
        value={'o'}
      />
    }
  >
    {'Other'}
  </FormControl>
</Box>;
```

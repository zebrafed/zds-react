// @flow
import * as React from 'react';
import { useState, useCallback, useContext } from 'react';
import type { ComponentType } from 'react';
import styled, { css, ThemeContext } from 'styled-components';
import type { ComponentWithTheme } from '@src/theme';
import Box from '@src/components/Common/Box';
import Flex from '@src/components/Common/Flex';
import Color from 'color';
import Ripple from '@src/components/Common/Ripple';

type PropsRadio = {
  /**Checks the radio button.*/
  checked?: boolean,
  /**A callback function that is fired when the checked state of the radio button is changed.*/
  onChange?: (checked: boolean, value: any) => void,
  /**Disables the radio button.*/
  disabled?: boolean,
  /**A string assigned to the radio button which is passed through to the onChange function.*/
  value?: string,
  /** */
  name?: string,
};

type PropsRippleEffect = {
  checked?: boolean,
};

type PropsContainer = {
  children?: React.Node,
};

const StyledRadio = styled(Box)`
  height: 24px;
  width: 24px;
  background-color: ${({ theme }) => theme.graySwatches[500]};
  border-radius: 50%;
  &:after {
    top: 4px;
    left: 4px;
    width: 16px;
    height: 16px;
    border-radius: 50%;
    background-color: ${({ theme }) => theme.white};
    content: '';
    position: absolute;
    border: none;
  }
`;

const HiddenRadio: ComponentType<PropsRadio> = styled.input.attrs({
  type: 'radio',
})`
  // Hide Radio visually but remain accessible to screen readers.
  // Source: https://polished.js.org/docs/#hidevisually
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`;

const RadioContainer: ComponentType<PropsContainer> = styled(Flex)`
  position: relative;
  ${HiddenRadio}:checked + ${StyledRadio} {
    &:after {
      top: 4px;
      left: 4px;
      width: 12px;
      height: 12px;
      border: 2px solid white;
      background-color: ${({ theme }) => theme.primaryColor};
    }
  }
  ${HiddenRadio}:disabled + ${StyledRadio} {
    background-color: ${({ theme }) => theme.graySwatches[300]};
    &:after {
      background-color: ${({ theme }) => theme.graySwatches[300]};
    }
  }
`;

const RadioWrap: ComponentType<PropsRadio> = ({ checked, ...props }) => {
  return (
    <RadioContainer>
      <HiddenRadio checked={checked} {...props} />
      <StyledRadio />
    </RadioContainer>
  );
};

/**
 * Radio buttons are used to select a single option from a list of exposed options.
 *
 * @example import { Radio } from '@badger/react';
 */
const Radio: ComponentType<PropsRadio> = (props: PropsRadio) => {
  const theme = useContext(ThemeContext);

  const [checked, setChecked] = useState(props.checked);

  const handleCheckboxChange = (event: any) => {
    let checked: boolean = event.target.checked;
    !props.name && setChecked(true);
    if (props.onChange) {
      props.onChange(checked, props.value);
    }
  };

  const memoizedCallback = useCallback(handleCheckboxChange, []);

  return (
    <Ripple
      disable={props.disabled}
      color={props.checked ? theme.primaryColor : theme.graySwatches[100]}
    >
      <RadioWrap
        disabled={props.disabled}
        name={props.name}
        onChange={memoizedCallback}
        checked={props.name ? props.checked : checked}
      />
    </Ripple>
  );
};

export default Radio;

# Mobile Drawer with large header

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import { CardHeader } from '@badger/react/components/Card';
import {
  Box,
  Drawer,
  DrawerHeader,
  DrawerList,
  Avatar,
  Icon,
  DrawerMenuItem,
} from '@badger/react';
import Hand from '@zebra/icons/enterprise/icons/hand.svg';
import '@zebra/icons/general/index.css';
import '@zebra/icons/weather/index.css';
import styled from 'styled-components';

<>
  <Box width={1} height={566} />
  <Drawer mobile>
    <DrawerHeader>
      <Avatar as={'img'} src={'https://i.pravatar.cc/150?img=12'} />
      <CardHeader title={'Name Surname'} subheader={'Details'} />
    </DrawerHeader>
    <DrawerList>
      <DrawerMenuItem
        onClick={() => console.log('person')}
        leading={<Icon category={'general'} name={'person'} />}
      >
        {'Account'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Hand />}>{'Privacy'}</DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'star'} />}>
        {'Favorites'}
      </DrawerMenuItem>
      <DrawerMenuItem
        leading={<Icon category={'general'} name={'documents'} />}
      >
        {'Documents'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'weather'} name={'cloudy'} />}>
        {'Cloud'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'settings'} />}>
        {'Settings'}
      </DrawerMenuItem>
    </DrawerList>
  </Drawer>
</>;
```

# Mobile Drawer with large header and menu divider with subheader

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import { CardHeader } from '@badger/react/components/Card';
import {
  Box,
  Drawer,
  DrawerHeader,
  DrawerList,
  Avatar,
  Icon,
  DrawerMenuItem,
  DrawerMenuDivider,
} from '@badger/react';
import Hand from '@zebra/icons/enterprise/icons/hand.svg';
import '@zebra/icons/general/index.css';
import '@zebra/icons/weather/index.css';
import styled from 'styled-components';

<>
  <Box width={1} height={566} />
  <Drawer mobile>
    <DrawerHeader>
      <Avatar as={'img'} src={'https://i.pravatar.cc/150?img=12'} />
      <CardHeader title={'Name Surname'} subheader={'Details'} />
    </DrawerHeader>
    <DrawerList>
      <DrawerMenuItem
        onClick={() => console.log('person')}
        leading={<Icon category={'general'} name={'person'} />}
      >
        {'Account'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Hand />}>{'Privacy'}</DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'star'} />}>
        {'Favorites'}
      </DrawerMenuItem>
      <DrawerMenuItem
        leading={<Icon category={'general'} name={'documents'} />}
      >
        {'Documents'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'weather'} name={'cloudy'} />}>
        {'Cloud'}
      </DrawerMenuItem>
      <DrawerMenuDivider title={'Subheader'} />
      <DrawerMenuItem leading={<Icon category={'general'} name={'settings'} />}>
        {'Settings'}
      </DrawerMenuItem>
    </DrawerList>
  </Drawer>
</>;
```

# Mobile Portable Drawer

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import {
  Drawer,
  DrawerHeader,
  DrawerList,
  DrawerMenuItem,
} from '@badger/react';
import { Icon, Avatar, Box } from '@badger/react/components/Common';
import Hand from '@zebra/icons/enterprise/icons/hand.svg';
import '@zebra/icons/general/index.css';
import '@zebra/icons/weather/index.css';
import styled from 'styled-components';

<>
  <Box width={1} height={566} />
  <Drawer portable mobile>
    <DrawerHeader>
      <Avatar as={'img'} src={'https://i.pravatar.cc/150?img=12'} />
    </DrawerHeader>
    <DrawerList>
      <DrawerMenuItem
        onClick={() => console.log('person')}
        leading={<Icon category={'general'} name={'person'} />}
      >
        {'Account'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Hand />}>{'Privacy'}</DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'star'} />}>
        {'Favorites'}
      </DrawerMenuItem>
      <DrawerMenuItem
        leading={<Icon category={'general'} name={'documents'} />}
      >
        {'Documents'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'weather'} name={'cloudy'} />}>
        {'Cloud'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'settings'} />}>
        {'Settings'}
      </DrawerMenuItem>
    </DrawerList>
  </Drawer>
</>;
```

# Mobile Portable Drawer with menu divider

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import {
  Drawer,
  DrawerHeader,
  DrawerList,
  DrawerMenuItem,
  DrawerMenuDivider,
} from '@badger/react';
import { Icon, Avatar, Box } from '@badger/react/components/Common';
import Hand from '@zebra/icons/enterprise/icons/hand.svg';
import '@zebra/icons/general/index.css';
import '@zebra/icons/weather/index.css';
import styled from 'styled-components';

<>
  <Box width={1} height={566} />
  <Drawer portable mobile>
    <DrawerHeader>
      <Avatar as={'img'} src={'https://i.pravatar.cc/150?img=12'} />
    </DrawerHeader>
    <DrawerList>
      <DrawerMenuItem
        onClick={() => console.log('person')}
        leading={<Icon category={'general'} name={'person'} />}
      >
        {'Account'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Hand />}>{'Privacy'}</DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'star'} />}>
        {'Favorites'}
      </DrawerMenuItem>
      <DrawerMenuItem
        leading={<Icon category={'general'} name={'documents'} />}
      >
        {'Documents'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'weather'} name={'cloudy'} />}>
        {'Cloud'}
      </DrawerMenuItem>
      <DrawerMenuDivider />
      <DrawerMenuItem leading={<Icon category={'general'} name={'settings'} />}>
        {'Settings'}
      </DrawerMenuItem>
    </DrawerList>
  </Drawer>
</>;
```

# Mobile Drawer with small header

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import { CardHeader } from '@badger/react/components/Card';
import {
  Box,
  Drawer,
  DrawerHeader,
  DrawerList,
  Avatar,
  Icon,
  DrawerMenuItem,
} from '@badger/react';
import Hand from '@zebra/icons/enterprise/icons/hand.svg';
import '@zebra/icons/general/index.css';
import '@zebra/icons/weather/index.css';
import styled from 'styled-components';

<>
  <Box width={1} height={566} />
  <Drawer mobile>
    <DrawerHeader small>
      <Avatar as={'img'} src={'https://i.pravatar.cc/150?img=12'} />
      <CardHeader title={'Name Surname'} subheader={'Details'} />
    </DrawerHeader>
    <DrawerList>
      <DrawerMenuItem
        onClick={() => console.log('person')}
        leading={<Icon category={'general'} name={'person'} />}
      >
        {'Account'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Hand />}>{'Privacy'}</DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'star'} />}>
        {'Favorites'}
      </DrawerMenuItem>
      <DrawerMenuItem
        leading={<Icon category={'general'} name={'documents'} />}
      >
        {'Documents'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'weather'} name={'cloudy'} />}>
        {'Cloud'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'settings'} />}>
        {'Settings'}
      </DrawerMenuItem>
    </DrawerList>
  </Drawer>
</>;
```

# Mobile Drawer Example with small header and divider

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import { CardHeader } from '@badger/react/components/Card';
import {
  Box,
  Drawer,
  DrawerHeader,
  DrawerList,
  Avatar,
  Icon,
  DrawerMenuItem,
  DrawerMenuDivider,
} from '@badger/react';
import Hand from '@zebra/icons/enterprise/icons/hand.svg';
import '@zebra/icons/general/index.css';
import '@zebra/icons/weather/index.css';
import styled from 'styled-components';

<>
  <Box width={1} height={566} />
  <Drawer mobile>
    <DrawerHeader small>
      <Avatar as={'img'} src={'https://i.pravatar.cc/150?img=12'} />
      <CardHeader title={'Name Surname'} subheader={'Details'} />
    </DrawerHeader>
    <DrawerList>
      <DrawerMenuItem
        onClick={() => console.log('person')}
        leading={<Icon category={'general'} name={'person'} />}
      >
        {'Account'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Hand />}>{'Privacy'}</DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'star'} />}>
        {'Favorites'}
      </DrawerMenuItem>
      <DrawerMenuItem
        leading={<Icon category={'general'} name={'documents'} />}
      >
        {'Documents'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'weather'} name={'cloudy'} />}>
        {'Cloud'}
      </DrawerMenuItem>
      <DrawerMenuDivider title={'Subheader'} />
      <DrawerMenuItem leading={<Icon category={'general'} name={'settings'} />}>
        {'Settings'}
      </DrawerMenuItem>
    </DrawerList>
  </Drawer>
</>;
```

// @flow
import * as React from 'react';
import Flex from '@src/components/Common/Flex';
import styled, { css } from 'styled-components';
import type { ComponentType } from 'react';
import { CardHeaderWrap, SubHeading, Heading } from '../Card/CardHeader';
import { DrawerMenuItemWrapper } from './DrawerMenuItem';

const DrawerList: ComponentType<{ children: React.Node }> = styled(Flex).attrs({
  flex: 1,
  /* py: 16,
  px: 4, */
  flexDirection: 'column',
})`
  ${DrawerMenuItemWrapper}:first-child {
    padding-top: 16px;
    ${({ theme }) => theme.lg`
    padding-top: 9px;
    `}
  }
`;

export default DrawerList;

# Empty Drawer Example

An empty drawer.

```js
import { Drawer, Box } from '@badger/react';
import styled from 'styled-components';

<>
  <Box width={1} height={403}/>
  <Drawer />
</>;
```

# Drawer Example

A drawer with some example content and a picture avatar.

```js
import { CardHeader } from '@badger/react/components/Card';
import {
  Box,
  Drawer,
  DrawerHeader,
  DrawerList,
  Avatar,
  Icon,
  DrawerMenuItem,
} from '@badger/react';
import '@zebra/icons/general/index.css';
import '@zebra/icons/weather/index.css';
import styled from 'styled-components';


<>
  <Box width={1} height={403} />
  <Drawer>
    <DrawerHeader>
      <Avatar as={'img'} src={'https://i.pravatar.cc/150?img=12'} />
      <CardHeader title={'Name Surname'} subheader={'Details'} />
    </DrawerHeader>
    <DrawerList>
      <DrawerMenuItem
        onClick={() => console.log('person')}
        leading={<Icon category={'general'} name={'person'} />}
      >
        {'Account'}
      </DrawerMenuItem>   
      <DrawerMenuItem leading={<Icon category={'general'} name={'star'} />}>
        {'Favorites'}
      </DrawerMenuItem>
      <DrawerMenuItem
        leading={<Icon category={'general'} name={'documents'} />}
      >
        {'Documents'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'weather'} name={'cloudy'} />}>
        {'Cloud'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'settings'} />}>
        {'Settings'}
      </DrawerMenuItem>      
    </DrawerList>
  </Drawer>
</>;
```

# Drawer example 2

A drawer with some different example content and a text avatar.

```js
import { CardHeader } from '@badger/react/components/Card';
import {
  Drawer,
  DrawerHeader,
  DrawerList,
  DrawerAccounts,
  DrawerMenuItem,
  Icon,
  Avatar,
  Box,
} from '@badger/react';
import '@zebra/icons/general/index.css';
import '@zebra/icons/weather/index.css';
import styled from 'styled-components';

<>
  <Box width={1} height={403} />
  <Drawer>
    <DrawerHeader>
      <DrawerAccounts>
        <Avatar>{'NS'}</Avatar>
        <Avatar>{'N'}</Avatar>
      </DrawerAccounts>
      <CardHeader title={'Name Surname'} subheader={'Details'} />
    </DrawerHeader>
    <DrawerList>
      <DrawerMenuItem
        onClick={() => console.log('person')}
        leading={<Icon category={'general'} name={'person'} />}
      >
        {'Account'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'star'} />}>
        {'Favorites'}
      </DrawerMenuItem>
      <DrawerMenuItem
        leading={<Icon category={'general'} name={'documents'} />}
      >
        {'Documents'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'weather'} name={'cloudy'} />}>
        {'Cloud'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'settings'} />}>
        {'Settings'}
      </DrawerMenuItem>
    </DrawerList>
  </Drawer>
</>;
```

# Drawer Example 3

A drawer with some example content and text and picture avatars.

```js
import { CardHeader } from '@badger/react/components/Card';
import {
  Drawer,
  DrawerHeader,
  DrawerList,
  DrawerAccounts,
  DrawerMenuItem,
} from '@badger/react';
import { Icon, Avatar, Box } from '@badger/react/components/Common';
import '@zebra/icons/general/index.css';
import '@zebra/icons/weather/index.css';
import styled from 'styled-components';

<>
  <Box width={1} height={403} />
  <Drawer>
    <DrawerHeader>
      <DrawerAccounts>
        <Avatar>{'NS'}</Avatar>
        <Avatar>{'N'}</Avatar>
        <Avatar as={'img'} src={'https://i.pravatar.cc/150?img=12'} />
      </DrawerAccounts>
      <CardHeader title={'Name Surname'} subheader={'Details'} />
    </DrawerHeader>
    <DrawerList>
      <DrawerMenuItem
        onClick={() => console.log('person')}
        leading={<Icon category={'general'} name={'person'} />}
      >
        {'Account'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'star'} />}>
        {'Favorites'}
      </DrawerMenuItem>
      <DrawerMenuItem
        leading={<Icon category={'general'} name={'documents'} />}
      >
        {'Documents'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'weather'} name={'cloudy'} />}>
        {'Cloud'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'settings'} />}>
        {'Settings'}
      </DrawerMenuItem>
    </DrawerList>
  </Drawer>
</>;
```

# Portable Drawer Example

Portable drawers are typically used for icon menus. This is a portable drawer with some example icons.

```js
import {
  Drawer,
  DrawerHeader,
  DrawerList,
  DrawerMenuItem,
} from '@badger/react';
import { Icon, Avatar, Box } from '@badger/react/components/Common';
import '@zebra/icons/general/index.css';
import '@zebra/icons/weather/index.css';
import styled from 'styled-components';

<>
  <Box width={1} height={403} />
  <Drawer portable>
    <DrawerHeader>
      <Avatar as={'img'} src={'https://i.pravatar.cc/150?img=12'} />
    </DrawerHeader>
    <DrawerList>
      <DrawerMenuItem
        onClick={() => console.log('person')}
        leading={<Icon category={'general'} name={'person'} />}
      >
        {'Account'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'star'} />}>
        {'Favorites'}
      </DrawerMenuItem>
      <DrawerMenuItem
        leading={<Icon category={'general'} name={'documents'} />}
      >
        {'Documents'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'weather'} name={'cloudy'} />}>
        {'Cloud'}
      </DrawerMenuItem>
      <DrawerMenuItem leading={<Icon category={'general'} name={'settings'} />}>
        {'Settings'}
      </DrawerMenuItem>
    </DrawerList>
  </Drawer>
</>;
```

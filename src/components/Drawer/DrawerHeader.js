// @flow
import * as React from 'react';
import Flex from '@src/components/Common/Flex';
import styled, { css } from 'styled-components';
import type { ComponentType } from 'react';
import { CardHeaderWrap, SubHeading, Heading } from '../Card/CardHeader';
import Avatar from '../Common/Avatar';

type BaseThemedHeading = {
  fontWeigth: Number,
  color: String,
  lineHeight: String,
  small: boolean,
};

const HeadingsStyles: ComponentType<BaseThemedHeading> = css`
  font-weight: 500;
  color: ${({ theme }) => theme.white};
  line-height: normal;
  ${({ theme }) => theme.lg`
      font-size: 14px;
      line-height: 1.43;
    `}
`;

const DrawerHeader: ComponentType<{ children: React.Node }> = styled(
  Flex
).attrs({
  flex: 1,
  py: 16,
  px: 4,
})`
  ${({ small }) => small && 'padding-bottom: 11px !important;'}
  user-select: none;
  background-color: ${({ theme }) => theme.primaryColor};
  flex-direction: ${({ small }) => (small ? 'row' : 'column')};
  ${Avatar} {
    ${({ small }) =>
    small &&
    'width: 40px !important; height: 40px !important; line-height: 40px !important;'};
  }
  ${CardHeaderWrap} {
    margin-top: ${({ small }) => (small ? 0 : 12)}px;
    border-bottom: none;
    padding: 0;
    min-height: 37px;
    ${({ small }) => small && 'margin-left: 16px'};
    ${({ theme, small }) => theme.lg`
    margin-top: ${small ? 0 : 25}px;
    margin-left: ${small && '16px'};
    `}
    ${Flex} {
      margin: 0;
    }
  }
  ${Heading} {
    ${HeadingsStyles}
  }
  ${SubHeading} {
    ${HeadingsStyles}
    font-size: 12px;
    padding-top: 4px;
    ${({ theme }) => theme.lg`
      font-size: 14px;
      line-height: 1.43;
      font-weight: normal;
    `}
  }
`;

export default DrawerHeader;

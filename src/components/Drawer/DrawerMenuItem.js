// @flow
import * as React from 'react';
import Flex from '@src/components/Common/Flex';
import styled from 'styled-components';
import Text from '@src/components/Common/Text';
import type { TextProps } from '@src/components/Common/Text';
import type { ComponentType } from 'react';
import Color from 'color';

type Props = {
  leading: React.Node,
  children: React.Node,
  onClick?: (e: any) => void,
};

export const DrawerMenuItemHeading: ComponentType<{
  children: React.Node,
}> = styled(Text)`
  font-size: 12px;
  font-weight: 500;
  color: ${({ theme }) => theme.graySwatches[800]};
  line-height: normal;
  padding-left: 16px;
  transition: all 0.3s;
  ${({ theme }) => theme.lg`
    font-size: 14px;
    line-height: 1.57;
  `}
`;

export const DrawerMenuItemLeading: ComponentType<{
  children: React.Node,
}> = styled(Flex)`
  svg {
    width: 24px;
    height: 24px;
    fill: ${({ theme }) => theme.graySwatches[500]};
    transition: all 0.3s;
  }
  color: ${({ theme }) => theme.graySwatches[500]};
  transition: all 0.3s;
`;

export const DrawerMenuItemWrapper: ComponentType<{
  children: React.Node,
}> = styled(Flex)`
  flex-direction: row;
  align-items: center;
  padding-left: 8px;
  padding-bottom: 8px;
  padding-top: 8px;
  ${({ theme }) => theme.lg`
    margin-left: 8px;
    margin-bottom: 9px;
    margin-top: 9px;
    margin-right: 8px;
    &:active {
      background-color: rgba(0, 122, 186, 0.1);
      ${DrawerMenuItemHeading} {
        color: unset;
      }
    }
    `}
  &:active {
    ${DrawerMenuItemHeading} {
      color: ${({ theme }) => theme.primaryColor};
    }
    ${DrawerMenuItemLeading} {
      svg {
        fill: ${({ theme }) => theme.primaryColor};
      }
      color: ${({ theme }) => theme.primaryColor};
    }
  }
`;

const DrawerMenuItem: ComponentType<Props> = ({
  leading,
  children,
  onClick,
}: Props) => {
  return (
    <DrawerMenuItemWrapper onClick={onClick}>
      <DrawerMenuItemLeading>{leading}</DrawerMenuItemLeading>
      <DrawerMenuItemHeading>{children}</DrawerMenuItemHeading>
    </DrawerMenuItemWrapper>
  );
};
export default DrawerMenuItem;

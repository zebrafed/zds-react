// @flow
import type { ComponentType } from 'react';
import { useContext } from 'react';
import { useState, useRef, useEffect } from 'react';
import * as React from 'react';
import styled, { ThemeContext } from 'styled-components';
import type { ComponentWithTheme } from '@src/theme';
import Color from 'color';
import Box from '@src/components/Common/Box';
import { range } from '@src/components/Common/Swipeable';
import { useGesture } from 'react-use-gesture';
import {
  DrawerMenuItemHeading,
  DrawerMenuItemLeading,
  DrawerMenuItemWrapper,
} from './DrawerMenuItem';
import useWindowDimensions from '../Common/UseWindowSizeHook';
import Avatar from '../Common/Avatar';
import DrawerHeader from './DrawerHeader';
import { StyledText } from './DrawerMenuDivider';

type Props = {
  /**Contains all the Drawer's sub-components.*/
  children?: React.Node,
  /**Decreases the navbar's max-width.*/
  portable?: boolean,
  /**Causes the navbar fill the screen (when it's not made portable).*/
  mobile?: boolean,
};

type OverlayProps = {
  alpha: number,
};

const DrawerOverlay: ComponentType<OverlayProps> = styled(Box).attrs(
  ({ theme, alpha = 1 }) => ({
    style: {
      backgroundColor: Color(theme.black)
        .alpha(alpha)
        .toString(),
    },
  })
)`
  width: 100%;
  height: 403px;
  width: 240px;
  top: 0;
  left: 0;
  position: fixed;
  overflow: hidden;
  pointer-events: ${({ translation }) => (translation > 0 ? 'unset' : 'none')};
  ${DrawerMenuItemHeading} {
    display: ${({ portable }) => portable && 'none'};
  }
  ${DrawerMenuItemLeading} {
    margin: ${({ portable }) => (portable ? '0 auto' : 'unset')};
  }

  ${DrawerMenuItemWrapper} {
    ${({ portable }) => portable && 'padding-left: 0px;'};
  }
  ${DrawerHeader} {
    ${({ theme, portable }) => theme.lg`
      padding: ${portable ? '16px 8px' : '16px 16px 19px 16px'};
  `}
  }
  ${Avatar} {
    ${({ theme, portable }) => theme.lg`
      width: ${portable ? 40 : 64}px;
      height: ${portable ? 40 : 64}px;
      line-height: ${portable ? 40 : 64}px;
    `}
  }

  ${StyledText} {
    display: ${({ portable }) => portable && 'none'};
  }

  ${({ theme }) => theme.lg`
    height: 566px;
    width: 360px;
  `}
`;

const DrawerMenu: ComponentType<Props> = styled(Box).attrs(
  ({ translation = 0, maxMenuWidth }) => ({
    style: {
      transform: `translateX(${-(maxMenuWidth - translation)}px)`,
    },
  })
)`
  background-color: ${({ theme }) => theme.white};
  height: 403px;
  width: ${({ maxMenuWidth }) => maxMenuWidth + 'px'};
  ${({ animate }) => animate && `transition: all 0.3s;`}
  ${({ theme }) => theme.lg`
    height: 566px;
  `}
`;

const isDrawerActive = (initialY: number, size: any): boolean =>
  initialY > size.top && initialY < size.bottom;

/**
 * Navigation drawers provide access to destinations and app functionality. 
 * They can either be permanently on-screen or controlled by a navigation menu icon.
 * Click and drag to the right on the side of the grey box to open the draw (on mobile this would be a swipe).
 * 
 * @example import { Drawer } from '@badger/react';
 */
const Drawer: ComponentType<Props> = ({
  portable,
  children,
  mobile,
  ...props
}: Props) => {
  const [down, setDown] = useState(false);
  const [translation, setTranslation] = useState(0);
  const { height, width } = useWindowDimensions();
  const drawer = useRef();
  const theme = useContext(ThemeContext);
  let maxMenuWidth;
  if (theme.demo) {
    maxMenuWidth = portable ? (mobile ? 56 : 64) : mobile ? 296 : 208;
  } else {
    maxMenuWidth = portable ? (width > 300 ? 56 : 64) : width > 300 ? 296 : 208;
  }

  const bind = useGesture(
    {
      onAction: ({
        down,
        event,
        initial: [initialX, initialY],
        delta: [deltaX],
        ...rest
      }) => {
        const el: any = drawer.current;
        const sizeBox = el.getBoundingClientRect();
        const target = event.target.closest('.demo-body') || document.body;
        const targetLeft = target.getBoundingClientRect().left;
        if (
          initialX - targetLeft < 15 &&
          deltaX < maxMenuWidth &&
          isDrawerActive(initialY, sizeBox)
        ) {
          setTranslation(deltaX);
        }
        setDown(down);

        if (!down) {
          if (!isDrawerActive(initialY, sizeBox)) {
            return;
          }
          if (deltaX > maxMenuWidth / 2 && initialX - targetLeft < 15) {
            setTranslation(maxMenuWidth);
          } else {
            setTranslation(0);
          }
        }
      },
    },
    { domTarget: document.body, event: { passive: false } }
  );
  useEffect(bind, [bind, drawer]);

  let alpha = range(0, maxMenuWidth * 1.65, translation);
  return (
    <DrawerOverlay
      ref={drawer}
      portable={portable}
      alpha={alpha}
      translation={translation}
    >
      <DrawerMenu
        onMouseUp={(e) => e.stopPropagation()}
        onMouseMove={(e) => e.stopPropagation()}
        onTouchMove={(e) => e.stopPropagation()}
        onTouchEnd={(e) => e.stopPropagation()}
        animate={!down}
        maxMenuWidth={maxMenuWidth}
        translation={translation}
      >
        {children}
      </DrawerMenu>
    </DrawerOverlay>
  );
};

export default Drawer;

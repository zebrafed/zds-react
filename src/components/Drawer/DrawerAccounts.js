// @flow
import * as React from 'react';
import { Component } from 'react';
import type { ComponentType } from 'react';
import styled from 'styled-components';
import type { ComponentWithTheme } from '@src/theme';
import Flex from '@src/components/Common/Flex';
import Avatar from '@src/components/Common/Avatar';

type ContentProps = {
  children: React.Node,
};

const DrawerAccounts: ComponentType<ContentProps> = styled(Flex)`
  font-family: Roboto;
  ${Avatar}:not(:first-child) {
    width: 24px;
    height: 24px;
    line-height: 24px;
    margin-left: 4px;
  }
  ${Avatar}:first-child {
    margin-right: auto;
    font-size: 28px;
  }
`;

export default DrawerAccounts;

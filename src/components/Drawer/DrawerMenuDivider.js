// @flow
import * as React from 'react';
import type { ComponentType } from 'react';
import styled from 'styled-components';
import Text from '../Common/Text';

type Props = {
  title: string,
};

const DividerWrap = styled.div`
  border-top: 1px solid ${({ theme }) => theme.graySwatches[50]};
  display: inline-flex;
`;

export const StyledText = styled(Text)`
  color: ${({ theme }) => theme.primaryColor};
  font-size: ${({ theme }) => theme.fontSizes[1]}px;
  font-weight: ${({ theme }) => theme.fontWeights[1]};
  margin-top: ${({ theme }) => theme.space[4]}px;
  margin-left: ${({ theme }) => theme.space[4]}px;
`;

const DrawerMenuDivider: ComponentType<Props> = ({ title }: Props) => {
  return <DividerWrap>{title && <StyledText>{title}</StyledText>}</DividerWrap>;
};
export default DrawerMenuDivider;

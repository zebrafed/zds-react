// @flow
import type { ComponentType } from 'react';
import * as React from 'react';
import styled from 'styled-components';
import type { ComponentWithTheme } from '@src/theme';
import Color from 'color';
import Box from '@src/components/Common/Box';

type Props = {
  children: React.Node,
  onClick: () => void,
};

const MenuItem: ComponentType<Props> = styled(Box).attrs({
  paddingLeft: 4,
  paddingRight: 3,
  py: 4,
  fontSize: 0,
})`
  font-family: Roboto;
  cursor: pointer;
  color: ${({ theme }) => theme.graySwatches[700]};
  ${({ theme }) => theme.lg`
    padding-top: 14px;
    padding-bottom: 13px;
    font-size: ${theme.fontSizes[1]}px;
    line-height: 1.5;
  `}
`;

export default MenuItem;

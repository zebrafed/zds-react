# Basic Mobile Dropdown Example

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import { CardHeader } from '@badger/react/components/Card';
import { Dropdown, MenuItem } from '@badger/react';

const Example = () => {
  const [isOpen, setIsOpen] = React.useState(false);

  const handleClose = () => setIsOpen(false);
  const handleOpen = () => setIsOpen(true);

  return (
    <Dropdown
      header={<CardHeader title={'Dropdown'} />}
      open={isOpen}
      onOpen={handleOpen}
      onClose={handleClose}
    >
      <MenuItem onClick={handleClose}>{'Option 1'}</MenuItem>
      <MenuItem onClick={handleClose}>{'Option 2'}</MenuItem>
      <MenuItem onClick={handleClose}>{'Option 3'}</MenuItem>
    </Dropdown>
  );
};
<Example />;
```

# Mobile Dropdown Example with already selected value

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```js
import { CardHeader } from '@badger/react/components/Card';
import { Dropdown, MenuItem } from '@badger/react';

const Example = () => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [selected, setSelected] = React.useState('Dropdown');

  const handleClose = () => setIsOpen(false);
  const handleSelect = (value) => {
    setSelected(value);
    setIsOpen(false);
  };
  const handleOpen = () => setIsOpen(true);

  return (
    <Dropdown
      selected={!!selected}
      header={<CardHeader title={'Type:'} subheader={selected} />}
      open={isOpen}
      onOpen={handleOpen}
      onClose={handleClose}
    >
      <MenuItem onClick={() => handleSelect('Option 1')}>{'Option 1'}</MenuItem>
      <MenuItem onClick={() => handleSelect('Option 2')}>{'Option 2'}</MenuItem>
      <MenuItem onClick={() => handleSelect('Option 3')}>{'Option 3'}</MenuItem>
    </Dropdown>
  );
};
<Example />;
```

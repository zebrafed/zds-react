# Basic Dropdown Example

A basic dropdown menu with three menu options.

```js
import { CardHeader } from '@badger/react/components/Card';
import { Dropdown, MenuItem } from '@badger/react';

const Example = () => {
  const [isOpen, setIsOpen] = React.useState(false);

  const handleClose = () => setIsOpen(false);
  const handleOpen = () => setIsOpen(true);

  return (
    <Dropdown
      header={<CardHeader title={'Type:'} />}
      open={isOpen}
      onOpen={handleOpen}
      onClose={handleClose}
    >
      <MenuItem onClick={handleClose}>
        {'Show all notification lorem ipsum dolor sit amet'}
      </MenuItem>
      <MenuItem onClick={handleClose}>
        {'Hide sensitive lorem ipsum dolor sit amet'}
      </MenuItem>
      <MenuItem onClick={handleClose}>
        {'Don’t show notifications at all'}
      </MenuItem>
    </Dropdown>
  );
};
<Example />;
```

# Dropdown Example with already selected value

A dropdown menu with a header that displays the selected menu item.

```js
import { CardHeader } from '@badger/react/components/Card';
import { Dropdown, MenuItem } from '@badger/react';

const Example = () => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [selected, setSelected] = React.useState('Show all Notifications');

  const handleClose = () => setIsOpen(false);
  const handleSelect = (value) => {
    setSelected(value);
    setIsOpen(false);
  };
  const handleOpen = () => setIsOpen(true);

  return (
    <Dropdown
      selected={!!selected}
      header={<CardHeader title={'Type:'} subheader={selected} />}
      open={isOpen}
      onOpen={handleOpen}
      onClose={handleClose}
    >
      <MenuItem onClick={() => handleSelect('Show all Notifications')}>
        {'Show all notification lorem ipsum dolor sit amet'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Hide sensitive')}>
        {'Hide sensitive lorem ipsum dolor sit amet'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Don`t show notifications')}>
        {'Don’t show notifications at all'}
      </MenuItem>
    </Dropdown>
  );
};
<Example />;
```

# Dropdown Example with leading icon

A dropdown menu with a header that has a leading icon and displays the selected menu item.

```js
import { CardHeader } from '@badger/react/components/Card';
import { Dropdown, MenuItem, Icon } from '@badger/react';
import Hand from '@zebra/icons/enterprise/icons/hand.svg';
import { ThemeContext } from 'styled-components';

const theme = React.useContext(ThemeContext);

const Example = () => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [selected, setSelected] = React.useState('Show all Notifications');

  const handleClose = () => setIsOpen(false);
  const handleSelect = (value) => {
    setSelected(value);
    setIsOpen(false);
  };
  const handleOpen = () => setIsOpen(true);

  return (
    <Dropdown
      selected={!!selected}
      header={
        <CardHeader
          leading={<Hand width={24} height={24} fill={theme.primaryColor} />}
          title={'Type:'}
          subheader={selected}
        />
      }
      open={isOpen}
      onOpen={handleOpen}
      onClose={handleClose}
    >
      <MenuItem onClick={() => handleSelect('Show all Notifications')}>
        {'Show all notification lorem ipsum dolor sit amet'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Hide sensitive')}>
        {'Hide sensitive lorem ipsum dolor sit amet'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Don`t show notifications')}>
        {'Don’t show notifications at all'}
      </MenuItem>
    </Dropdown>
  );
};
<Example />;
```

# Dropdown Example with many options

A scrollable dropdown menu.

```js
import { CardHeader } from '@badger/react/components/Card';
import { Dropdown, MenuItem, Icon } from '@badger/react';
import Hand from '@zebra/icons/enterprise/icons/hand.svg';
import { ThemeContext } from 'styled-components';

const theme = React.useContext(ThemeContext);
const Example = () => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [selected, setSelected] = React.useState('Show all Notifications');

  const handleClose = () => setIsOpen(false);
  const handleSelect = (value) => {
    setSelected(value);
    setIsOpen(false);
  };
  const handleOpen = () => setIsOpen(true);

  return (
    <Dropdown
      selected={!!selected}
      header={
        <CardHeader
          leading={<Hand width={24} height={24} fill={theme.primaryColor} />}
          title={'Type:'}
          subheader={selected}
        />
      }
      open={isOpen}
      onOpen={handleOpen}
      onClose={handleClose}
    >
      <MenuItem onClick={() => handleSelect('Show all Notifications')}>
        {'Show all notification lorem ipsum dolor sit amet'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Hide sensitive')}>
        {'Hide sensitive lorem ipsum dolor sit amet'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Don`t show notifications')}>
        {'Don’t show notifications at all'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Don`t show notifications')}>
        {'Don’t show notifications at all'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Don`t show notifications')}>
        {'Don’t show notifications at all'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Don`t show notifications')}>
        {'Don’t show notifications at all'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Don`t show notifications')}>
        {'Don’t show notifications at all'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Don`t show notifications')}>
        {'Don’t'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Don`t show notifications')}>
        {'Don’t show notifications at all'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Don`t show notifications')}>
        {'Don’t show notifications at all'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Don`t show notifications')}>
        {'Don’t show notifications at all'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Don`t show notifications')}>
        {'Don’t show notifications at all'}
      </MenuItem>
      <MenuItem onClick={() => handleSelect('Don`t show notifications')}>
        {'Don’t show notifications at all'}
      </MenuItem>
    </Dropdown>
  );
};
<Example />;
```



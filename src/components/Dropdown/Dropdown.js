// @flow
import type { ComponentType } from 'react';
import * as React from 'react';
import { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components';
import type { ComponentWithTheme } from '@src/theme';
import Color from 'color';
import Box from '@src/components/Common/Box';
import Flex from '@src/components/Common/Flex';
import Ripple, { RippleContainer } from '@src/components/Common/Ripple';
import {
  Heading,
  SubHeading,
  CardHeaderIcon,
  CardHeaderWrap,
} from '@src/components/Card/CardHeader';

type Props = {
  /**Content displayed inside the dropdown.*/
  header: React.Node,
  /**Opens and closes the dropdown menu. */
  open?: boolean,
  /**Used to indicate if a dropdown has had one of its items selected.*/
  selected?: boolean,
  /**Contains the dropdown menu items. */
  children?: React.Node,
  onOpen?: () => void,
  onClose?: () => void,
};

type ArrowWrapProps = {
  open?: boolean,
};

type ArrowProps = {
  selected?: boolean,
};

const ArrowWrap: ComponentType<ArrowWrapProps> = styled('span')`
  display: block;
  width: 16px;
  height: 16px;
  position: relative;
  top: 50%;
  transform-origin: 50% 50%;
  transform: ${(props) =>
    props.open
      ? 'rotate(180deg) translateY(4px)'
      : 'rotate(0deg) translateY(4px)'};
  ${({ theme }) => theme.lg`
    transform: ${(props) => (props.open ? 'rotate(-135deg)' : 'rotate(45deg)')};
  `}
`;

const Arrow: ComponentType<ArrowProps> = styled('span')`
  width: 0;
  height: 0;
  position: absolute;
  top: 0;
  left: 0;

  border-left: 8px solid transparent;
  border-right: 8px solid transparent;
  border-top: 8px solid
    ${(props) =>
    props.selected
      ? props.theme.primaryColor
      : props.theme.graySwatches[900]};

  ${({ theme }) => theme.lg`
    border: solid ${({ theme }) => theme.primaryColor};
    border-width: 0 2px 2px 0;
    padding: 5px;
    `}
`;

const DropdownStyles = styled(Box).attrs({
  m: 4,
})`
  cursor: pointer;
  position: relative;
  background-color: ${({ theme }) => theme.white};
  z-index: ${(props) => (props.open ? 100 : 1)};

  ${CardHeaderWrap} {
    background-color: ${({ theme }) => theme.white};
    position: relative;
    z-index: 10;
    padding-right: ${({ theme }) => theme.space[12]}px;
    padding-top: ${(props) =>
    props.selected
      ? props.theme.space[4] + 'px'
      : props.theme.space[6] + 'px'};
    padding-bottom: ${(props) =>
    props.selected
      ? props.theme.space[4] + 'px'
      : props.theme.space[6] + 'px'};

    ${({ theme }) => theme.lg`
      padding-top: ${(props) =>
      props.selected
        ? props.theme.space[6] + 'px'
        : props.theme.space[7] + 'px'};
      padding-bottom: ${(props) =>
      props.selected
        ? props.theme.space[5] + 'px'
        : props.theme.space[6] + 'px'};
          min-height: 48px;
      `}

    ${CardHeaderIcon} {
      margin-left: ${({ theme }) => theme.space[4]}px;
    }

    ${Heading} {
      font-size: ${(props) =>
    props.selected
      ? props.theme.fontSizes[0] + 'px'
      : props.theme.fontSizes[1] + 'px'};
      font-weight: ${(props) => props.theme.fontWeights[1]};
      margin-bottom: ${(props) =>
    props.selected
      ? props.theme.space[3] + 'px'
      : props.theme.space[0] + 'px'};
      color: ${(props) =>
    props.selected
      ? props.theme.primaryColor
      : props.theme.graySwatches[900]};
      ${({ theme }) => theme.lg`
      font-size: ${(props) =>
      props.selected ? '12px' : props.theme.fontSizes[1] + 'px'};
        line-height: 1.33;
          `}
    }
    ${SubHeading} {
      font-size: ${(props) =>
    props.selected
      ? props.theme.fontSizes[1] + 'px'
      : props.theme.fontSizes[0] + 'px'};
      color: ${(props) =>
    props.selected
      ? props.theme.graySwatches[800]
      : props.theme.graySwatches[500]};
      ${({ theme }) => theme.lg`
          font-weight: 500;
          `}
    }
    padding-left: 0;
    border-bottom-width: 2px;
    border-bottom-style: solid;
    border-bottom-color: ${(props) =>
    props.selected
      ? props.theme.primaryColor
      : props.open
        ? props.theme.graySwatches[300]
        : props.theme.graySwatches[900]};
    ${({ theme, selected }) => theme.lg`
          border-bottom: 1px solid  ${
    selected ? theme.primaryColor : theme.graySwatches[300]
    }
        `}
  }
  ${RippleContainer} {
    position: absolute;
    z-index: 11;
    right: 6px;
    top: 50%;
    transform: translateY(-50%);
  }
`;

export const MenuStyles: ComponentType<{ children?: React.Node }> = styled(
  Box
).attrs({})`
  overflow: auto;
  display: ${({ visible }) => (visible ? 'block' : 'none')};
  width: 100%;
  position: absolute;
  max-height: 160px;
  background-color: ${(props) => props.theme.white};
  box-shadow: ${(props) =>
    `0 2px 8px 2px ${Color(props.theme.black).alpha(0.5)}`};
`;
type MenuProps = {
  visible?: boolean,
  children?: React.Node,
};

export const Menu: ComponentType<MenuProps> = ({ visible, children }) => {
  return <MenuStyles visible={visible}>{children}</MenuStyles>;
};
/**
 * A dropdown menu is a compact way of displaying multiple choices. 
 * It appears upon interaction with an element (such as an icon or button) or when users perform a specific action.
 *
 * @example import { Dropdown } from '@badger/react';
 */
const Dropdown: ComponentType<Props> = ({
  header,
  open = false,
  selected = false,
  onOpen = () => { },
  onClose = () => { },
  children,
}: Props) => {
  const theme = useContext(ThemeContext);

  return (
    <DropdownStyles open={open} selected={selected} flex={1}>
      <Box position="relative" onClick={() => (open ? onClose() : onOpen())}>
        {header}{' '}
        <Ripple color={selected ? theme.primaryColor : theme.graySwatches[100]}>
          <ArrowWrap open={open}>
            <Arrow selected={selected} />
          </ArrowWrap>
        </Ripple>
      </Box>
      {open && (
        <Flex position="relative" flexDirection="column">
          <Menu visible={open}>{children}</Menu>
        </Flex>
      )}
    </DropdownStyles>
  );
};

export default Dropdown;

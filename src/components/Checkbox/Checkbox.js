import React, { useState, useCallback, useContext } from 'react';
import type { ComponentType, Node } from 'react';
import styled, { css, ThemeContext } from 'styled-components';
import type { ComponentWithTheme } from '@src/theme';
import Box from '@src/components/Common/Box';
import Flex from '@src/components/Common/Flex';
import Checked from '@zebra/icons/navigation/icons/check-box-alt.svg';
import Indeterminated from '@zebra/icons/navigation/icons/remove-alt.svg';
import Unchecked from '@zebra/icons/navigation/icons/empty-box.svg';
import Color from 'color';
import Ripple from '@src/components/Common/Ripple';

type PropsCheckbox = {
  /**A boolean that states the checkbox's initial checked state.*/
  checked?: boolean,
  /**A callback function that's fired when the checkbox's state changes.*/
  onChange?: (checked: boolean, value: any) => void,
  /**A boolean that disables the checkbox.*/
  disabled?: boolean,
  /**A boolean that puts the checkbox in a partially checked state.*/
  indeterminate?: boolean,
  /**Is 'selected' or 'unchecked' based on the checkbox's state.*/
  value: string,
};

type PropsRippleEffect = {
  checked?: boolean,
};

type PropsContainer = {
  children?: React.Node,
};

type PropsSvg = {
  disabled?: boolean,
};

export const HiddenCheckbox: ComponentType<PropsCheckbox> = styled.input.attrs({
  type: 'checkbox',
})`
  // Hide checkbox visually but remain accessible to screen readers.
  // Source: https://polished.js.org/docs/#hidevisually
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`;

const StyledChecked: ComponentType<PropsSvg> = styled(Checked)`
  fill: ${({ disabled, theme }) =>
    disabled ? theme.graySwatches[300] : theme.primaryColor};
`;

const StyledIndeterminated: ComponentType<PropsSvg> = styled(Indeterminated)`
  fill: ${({ disabled, theme }) =>
    disabled ? theme.graySwatches[300] : theme.primaryColor};
`;

const StyledUncheked: ComponentType<PropsSvg> = styled(Unchecked)`
  fill: ${({ disabled, theme }) =>
    disabled ? theme.graySwatches[300] : theme.graySwatches[500]};
  background-color: ${({ disabled, theme }) =>
    disabled ? theme.graySwatches[300] : 'transparent'};
`;

const CheckboxContainer: ComponentType<PropsContainer> = styled(Flex)`
  position: relative;

  svg {
    height: 24px;
    width: 24px;
  }
`;

const CheckboxWrap: ComponentType<PropsCheckbox> = ({
  checked,
  indeterminate,
  ...props
}) => {
  return (
    <CheckboxContainer>
      <HiddenCheckbox
        checked={checked}
        {...props}
        ref={(el) => el && (el.indeterminate = indeterminate)}
      />
      {checked ? (
        indeterminate ? (
          <StyledIndeterminated disabled={props.disabled} />
        ) : (
            <StyledChecked disabled={props.disabled} />
          )
      ) : indeterminate ? (
        <StyledIndeterminated disabled={props.disabled} />
      ) : (
            <StyledUncheked disabled={props.disabled} />
          )}
    </CheckboxContainer>
  );
};

/**
 * Checkboxes allow a user to select one or more items from a set. 
 * They can also be used as a toggle for a single option.
 *
 * @example import { Checkbox } from '@badger/react';
 */
const Checkbox: ComponentType<PropsCheckbox> = (props: PropsCheckbox) => {
  const [checked, setChecked] = useState(props.checked || false);
  const [value, setValue] = useState(props.value);
  const [showRipple, setShowRipple] = useState(false);
  const [indeterminate, setIndeterminate] = useState(props.indeterminate);

  const handleCheckboxChange = (event: any) => {
    let value = indeterminate
      ? 'selected'
      : event.target.checked
        ? 'selected'
        : 'unchecked';
    let checked: boolean = indeterminate ? true : event.target.checked;
    setChecked(checked);
    setValue(value);
    setIndeterminate(false);
    setShowRipple(true);
    setTimeout(() => setShowRipple(false), 400);
    if (props.onChange) {
      props.onChange(checked, value);
    }
  };

  const memoizedCallback = useCallback(handleCheckboxChange, [
    value,
    checked,
    indeterminate,
  ]);

  const theme = useContext(ThemeContext);

  return (
    <Ripple
      disable={props.disabled}
      color={
        checked || indeterminate ? theme.primaryColor : theme.graySwatches[100]
      }
    >
      <CheckboxWrap
        checked={checked}
        value={value}
        indeterminate={indeterminate}
        onChange={memoizedCallback}
        disabled={props.disabled}
        showRipple={showRipple}
      />
    </Ripple>
  );
};

export default Checkbox;

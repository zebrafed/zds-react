# Default Checkbox Example

Below are examples of all possible states that a checkbox can be in. The 'indeterminate' state is intended for master checkboxes (checkboxes which control the state of multiple different checkboxes) for when not all the boxes in a group have been checked.


```jsx
import { FormControl, Checkbox, Box } from '@badger/react';
<Box px={4} py={6}>
  <FormControl
    control={
      <Checkbox
        checked={false}
        onChange={(checked, value) => console.log(checked, value)}
        value={'unchecked'}
      />
    }
  >
    {'Checkbox unchecked'}
  </FormControl>
  <FormControl
    control={
      <Checkbox
        checked={true}
        onChange={(checked, value) => console.log(checked, value)}
        value={'selected'}
      />
    }
  >
    {'Checkbox selected'}
  </FormControl>
  <FormControl
    control={
      <Checkbox
        checked={true}
        indeterminate
        onChange={(checked, value) => console.log(checked, value)}
        value={'indeterminate'}
      />
    }
  >
    {'Checkbox indeterminate'}
  </FormControl>
  <FormControl
    control={
      <Checkbox
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Checkbox disabled'}
  </FormControl>
  <FormControl
    control={
      <Checkbox
        checked={true}
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Checkbox disabled'}
  </FormControl>
  <FormControl
    control={
      <Checkbox
        indeterminate
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Checkbox disabled'}
  </FormControl>
</Box>;
```

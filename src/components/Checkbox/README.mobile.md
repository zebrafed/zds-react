# Mobile Checkbox Example without border

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```jsx
import { FormControl, Checkbox, Box } from '@badger/react';
<Box px={4} py={6}>
  <FormControl
    noborder
    control={
      <Checkbox
        checked={false}
        onChange={(checked, value) => console.log(checked, value)}
        value={'unchecked'}
      />
    }
  >
    {'Checkbox unchecked'}
  </FormControl>
  <FormControl
    noborder
    control={
      <Checkbox
        checked={true}
        onChange={(checked, value) => console.log(checked, value)}
        value={'selected'}
      />
    }
  >
    {'Checkbox selected'}
  </FormControl>
  <FormControl
    noborder
    control={
      <Checkbox
        checked={true}
        indeterminate
        onChange={(checked, value) => console.log(checked, value)}
        value={'indeterminate'}
      />
    }
  >
    {'Checkbox indeterminate'}
  </FormControl>
  <FormControl
    noborder = {true}
    control={
      <Checkbox
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Checkbox disabled'}
  </FormControl>
  <FormControl
    noborder
    control={
      <Checkbox
        checked={true}
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Checkbox disabled'}
  </FormControl>
  <FormControl
    noborder
    control={
      <Checkbox
        indeterminate
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Checkbox disabled'}
  </FormControl>
</Box>;
```

# Mobile Checkbox Example

Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi sapiente veritatis nemo quo quas tempora, eligendi harum alias iste libero laudantium? Nisi veniam maiores ratione quasi, cum sequi tenetur sed!

```jsx
import { FormControl, Checkbox, Box } from '@badger/react';
<Box px={4} py={6}>
  <FormControl
    control={
      <Checkbox
        checked={false}
        onChange={(checked, value) => console.log(checked, value)}
        value={'unchecked'}
      />
    }
  >
    {'Checkbox unchecked'}
  </FormControl>
  <FormControl
    control={
      <Checkbox
        checked={true}
        onChange={(checked, value) => console.log(checked, value)}
        value={'selected'}
      />
    }
  >
    {'Checkbox selected'}
  </FormControl>
  <FormControl
    control={
      <Checkbox
        checked={true}
        indeterminate
        onChange={(checked, value) => console.log(checked, value)}
        value={'indeterminate'}
      />
    }
  >
    {'Checkbox indeterminate'}
  </FormControl>
  <FormControl
    control={
      <Checkbox
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Checkbox disabled'}
  </FormControl>
  <FormControl
    control={
      <Checkbox
        checked={true}
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Checkbox disabled'}
  </FormControl>
  <FormControl
    control={
      <Checkbox
        indeterminate
        disabled
        onChange={(checked, value) => console.log(checked, value)}
      />
    }
  >
    {'Checkbox disabled'}
  </FormControl>
</Box>;
```

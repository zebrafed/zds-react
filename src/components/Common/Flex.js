// @flow
import type { ComponentType } from 'react';
import * as React from 'react';
import Box from '@src/components/Common/Box';
import styled from 'styled-components';
type Props = {
  children?: React.Node,
};

const Flex: ComponentType<Props> = styled(Box)({
  display: 'flex',
});

export default Flex;

//@flow
import * as React from 'react';
import type { ComponentType } from 'react';
import { ThemeProvider } from 'styled-components';
import defaultTheme, { Theme } from '@src/theme';
import 'typeface-roboto';

type Props = {
  children: React.Node,
  theme: Theme,
};

const BadgerThemeProvider: ComponentType<Props> = ({
  children,
  theme = defaultTheme,
}: Props) => <ThemeProvider theme={theme}>{children}</ThemeProvider>;

export default BadgerThemeProvider;

// @flow
import styled from 'styled-components';
import * as React from 'react';
import type { ComponentType } from 'react';
import Box from '@src/components/Common/Box';

type AvatarProps = {
  theme: Object,
  children: React.Node,
};

const Avatar: ComponentType<AvatarProps> = styled(Box)`
  width: 48px;
  height: 48px;
  border-radius: 50%;
  background-color: ${({ theme }) => theme.white};
  color: ${({ theme }) => theme.primaryColor};
  line-height: 48px;
  text-align: center;
`;

export default Avatar;

// @flow
import * as React from 'react';
import type { ComponentType } from 'react';
import { useState, useCallback } from 'react';
import styled from 'styled-components';
import Box from '@src/components/Common/Box';
import Color from 'color';
import type { OnlyChildren } from '@src/components/Common/Swipeable';

const RippleEffect = styled(Box).attrs({
  width: 48,
  height: 48,
})`
  position: absolute;
  left: 50%;
  top: 50%;
  border-radius: 10000000px;
  background-color: ${({ color, theme }) =>
    Color(color || theme.primaryColor)
      .fade(0.5)
      .toString()};
  transition: all 0.3s linear;
  transform-origin: 50% 50%;

  ${({ state }) =>
    state == 'visible'
      ? 'transform: translate3d(-50%, -50%, 0) scale(1); opacity: 1; transition: all 0.3s linear;'
      : state == 'gone'
      ? 'transform: translate3d(-50%, -50%, 0) scale(1.5); opacity: 0; transition: all 0.3s linear;'
      : 'transform: translate3d(-50%, -50%, 0) scale(0); opacity: 0; transition: all 0.0s linear;'}
`;

export const RippleContainer: ComponentType<OnlyChildren> = styled(Box)`
  position: relative;
`;
const RippleContent: ComponentType<OnlyChildren> = styled(Box)`
  position: relative;
`;

type Props = {
  color?: string,
  disable?: boolean,
  children?: React.Node,
};

const useEvents = ({ up, down }, inputs = []) => {
  const downHandler = useCallback(down, inputs);
  const upHandler = useCallback(up, inputs);

  return {
    onMouseDown: downHandler,
    onTouchStart: downHandler,
    onMouseUp: upHandler,
    onTouchEnd: upHandler,
  };
};

const Ripple: ComponentType<Props> = ({ color, disable = false, children }) => {
  const [state, setState] = useState(null);

  const events = useEvents({
    down: () => {
      if (disable) {
        return;
      }
      setState('visible');
    },
    up: () => {
      if (disable) {
        return;
      }
      setState('gone');

      setTimeout(() => setState(null), 300);
    },
  });

  return (
    <RippleContainer {...events}>
      <RippleEffect color={color} state={state} />
      <RippleContent>{children}</RippleContent>
    </RippleContainer>
  );
};

export default Ripple;

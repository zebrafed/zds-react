// @flow
import styled from 'styled-components';
import * as React from 'react';
import type { ComponentType } from 'react';
import Box from '@src/components/Common/Box';

type Props = {
  name: string,
  color?: string,
  size?: number,
  category: string,
};

const IconWrap = styled(Box)`
  width: ${({ fontSize = 24 }) => fontSize}px;
  height: ${({ fontSize = 24 }) => fontSize}px;
  font-size: ${({ fontSize = 24 }) => fontSize}px;
  color: ${({ color }) => color};

  :before {
    font-family: ${({ fontName }) => fontName} !important;
    font-style: normal;
    font-weight: normal !important;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
`;

const Icon: ComponentType<Props> = ({ name, category, size, color }: Props) => {
  return (
    <IconWrap
      className={`icon-${name}`}
      color={color}
      fontSize={size}
      fontName={category}
    />
  );
};

export default Icon;

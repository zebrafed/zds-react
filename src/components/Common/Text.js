// @flow
import * as React from 'react';
import { space, layout, typography, flexbox, color } from 'styled-system';
import styled from 'styled-components';
import type { ComponentType } from 'react';

export type TextProps = {
  fontSize?: number,
  fontWeight?: number,
  fontFamily?: string,
  children: React.Node,
};

const Text: ComponentType<TextProps> = styled.span`
  font-family: Roboto;
  line-height: 1.29;
  ${space}
  ${layout}
  ${typography}
  ${flexbox}
  ${color}
`;

export default Text;

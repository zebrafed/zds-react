// @flow
import type { ComponentType } from 'react';
import * as React from 'react';
import {
  space,
  layout,
  typography,
  flexbox,
  position,
  color,
} from 'styled-system';
import styled from 'styled-components';

type Props = {
  children?: React.Node,
};

const Box: ComponentType<Props> = styled.div.attrs({ boxSizing: 'border-box' })`
  ${space}
  ${layout}
  ${typography}
  ${flexbox}
  ${position}
  ${color}
`;

export default Box;

// @flow
import type { ComponentType } from 'react';
import * as React from 'react';
import { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components';
import Flex from '@src/components/Common/Flex';
import Box from '@src/components/Common/Box';
import type { ComponentWithTheme } from '@src/theme';
import Text from '@src/components/Common/Text';

type Props = {
  control?: React.Node,
  reverse?: boolean,
  children: React.Node,
  description?: React.Node,
  noborder?: boolean,
};

const FormControlStyle: ComponentType<{
  children?: React.Node,
  reverse?: boolean,
}> = styled(Flex).attrs(({ reverse }) => ({
  justifyContent: reverse ? 'flex-end' : 'space-between',
  alignItems: 'center',
  minHeight: 48,
  px: 4,
  py: 6,
  flexDirection: reverse ? 'row-reverse' : 'row',
}))`
  box-sizing: border-box;
  background-color: ${({ theme }) => theme.white};
  border-bottom: 1px solid ${({ theme }) => theme.graySwatches[300]};
  ${({ noborder }) => noborder && 'border-bottom: none'};
  ${({ theme, description }) => theme.lg`
    ${description ? 'padding-top: 16px; padding-bottom: 13px;' : 'padding-top: 0px; padding-bottom: 0px; '}
  `}
  > ${Text} {
  padding-${ ({ reverse }) => (reverse ? 'left' : 'right')}: 8px;
  user-select: none;
  ${({ theme }) => theme.lg`
    line-height: 1.5;
  `}
}
`;

const FormControl: ComponentType<Props> = ({
  control,
  children,
  reverse,
  description,
  noborder
}: Props) => {
  const theme = useContext(ThemeContext);
  return (
    <FormControlStyle noborder={noborder} description={description} as={'label'} reverse={reverse}>
      <Text color={theme.graySwatches[800]}>
        {children}
        {description && (
          <Text as={Box} color={theme.graySwatches[500]} fontSize={0}>
            {description}
          </Text>
        )}
      </Text>
      {control}
    </FormControlStyle>
  );
};

export default FormControl;

// @flow
import type { ComponentType } from 'react';
import { space, layout, typography, flexbox } from 'styled-system';
import * as React from 'react';
import styled from 'styled-components';
import Text from '@src/components/Common/Text';

type Props = {
  children?: React.Node,
  color?: string,
  onClick?: (e: any) => void,
};

export const ActionItemStyle: ComponentType<{
  children?: React.Node,
  color?: string,
}> = styled(Text).attrs({
  px: 4,
  py: 5,
  fontWeight: 2,
  fontSize: 1,
  textAlign: 'right',
})`
  font-family: Roboto;
  text-transform: uppercase;
  cursor: pointer;
  color: ${({ theme }) => theme.primaryColor};
  background-color: ${({ color = 'transparent' }) => color};
`;

const ActionItem: ComponentType<Props> = ({
  color,
  children,
  onClick,
}: Props) => {
  return (
    <ActionItemStyle color={color} onClick={onClick}>
      {children}
    </ActionItemStyle>
  );
};

export default ActionItem;

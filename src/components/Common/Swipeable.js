//@flow
import { Fragment, useCallback, useState, useEffect, useRef } from 'react';
import * as React from 'react';
import type { ComponentType, Element } from 'react';
import styled from 'styled-components';
import Flex from '@src/components/Common/Flex';
import Box from '@src/components/Common/Box';
import { CardHeaderWrap } from '@src/components/Card/CardHeader';
import { ActionItemStyle } from '@src/components/Common/ActionItem';
import { useGesture } from 'react-use-gesture';

export const range = (min: number, max: number, value: number) =>
  (value - min) / (max - min);

export type OnlyChildren = {
  children?: React.Node,
};

const SwipeableWrapper: ComponentType<OnlyChildren> = styled(Flex).attrs({
  my: 6,
  ml: 4,
})`
  position: relative;
  overflow: hidden;
  flex: 1;

  > div:not(${Flex}) {
    display: flex;
    flex: 1;
    background: ${({ theme }) => theme.white};
    margin-right: 8px;
  }

  ${CardHeaderWrap} {
    padding-top: ${({ theme }) => theme.space[2]}px;
    padding-bottom: ${({ theme }) => theme.space[2]}px;
    padding-left: ${({ theme }) => theme.space[2]}px;
    ${({ theme }) => theme.lg`
    padding-top: ${({ theme }) => theme.space[10]}px;
    padding-bottom: ${({ theme }) => theme.space[5]}px;
    padding-left: ${({ theme }) => theme.space[4]}px;
    padding-right: ${({ theme }) => theme.space[4]}px;
    `}
  }
`;

type SwipeableContainerProps = OnlyChildren & {
  offset?: number,
  opacity?: number,
};

const SwipeableContainer: ComponentType<SwipeableContainerProps> = styled(Box)`
  position: relative;
  left: ${({ offset = 0 }: SwipeableContainerProps) => offset}px;
  z-index: 1;
  opacity: 1;

  ${({ offset = 0, opacity = 1 }) =>
    offset != 0 &&
    `&:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.6);
    opacity: ${opacity};
  }`}
`;

const SwipeableActions: ComponentType<OnlyChildren> = styled(Flex)`
  width: 100%;
  height: 100%;
  justify-content: flex-end;
  align-items: stretch;
  position: absolute;

  ${ActionItemStyle} {
    width: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    color: #fff;
  }
`;

type TapToRevealProps = {
  open?: boolean,
};

const TapToReveal: ComponentType<TapToRevealProps> = styled(Box)`
  width: 14px;
  height: 100%;
  position: absolute;
  right: -8px;
  top: 0;
  bottom: 0;
  z-index: 3;
  background: transparent !important;
  cursor: pointer;
  display: ${({ open }) => (open ? 'none' : 'flex')} !important;
`;

const _getContentEasing = (value: number, limit: number) => {
  // limit content style left when value > actions width
  const delta = Math.abs(value) - Math.abs(limit);
  const isOverflow = delta > 0;
  const factor = limit > 0 ? 1 : -1;
  if (isOverflow) {
    value = limit + Math.pow(delta, 0.85) * factor;
    return Math.abs(value) > Math.abs(limit) ? limit : value;
  }
  return value;
};

const _buttonReveal = 8;
const _buttonWidth = 56;
const _getButtonsWidth = (count = 0) => {
  return _buttonWidth * count - _buttonReveal;
};

const usePreventActions = (callback: (e: Event) => void) => {
  useEffect(() => {
    if (document.body == null) {
      return;
    }
    const body: HTMLBodyElement = document.body;

    body.addEventListener('touchstart', callback);
    body.addEventListener('mousedown', callback);

    return () => {
      body.removeEventListener('mousedown', callback);
      body.removeEventListener('touchstart', callback);
    };
  }, []);

  const movable: { current: React.ElementRef<any> | null } = useRef(null);

  useEffect(() => {
    if (movable.current == null) {
      return;
    }
    const current: HTMLElement = movable.current;

    current.addEventListener('touchstart', prevent, false);
    current.addEventListener('mousedown', prevent, false);

    return () => {
      current.removeEventListener('touchstart', prevent, false);
      current.removeEventListener('mousedown', prevent, false);
    };
  });

  const prevent: (e: Event) => void = useCallback((e: Event) => {
    e.stopPropagation();
  }, []);

  return [movable];
};

const getCount: (actions: any) => number = (actions) => {
  const _isFragment = actions.type == Fragment;

  return _isFragment && Array.isArray(actions.props.children)
    ? actions.props.children.length
    : 1;
};

type SwipeableProps = {
  children?: React.Node,
  actions?: React.Node,
};

const Swipeable: ComponentType<SwipeableProps> = ({
  children,
  actions,
}: SwipeableProps) => {
  let _swiping = false;
  let _opened = false;
  let count = getCount(actions);

  const _width = _getButtonsWidth(count);
  const [offset, setOffset] = useState(0);
  const [opened, setOpened] = useState(_opened);

  const onCloseSwipe = useCallback((e) => {
    if (!_opened) {
      return;
    }
    close();
  }, []);

  const close = useCallback((e) => {
    setOffset(0);
    setOpened((_opened = false));
  }, []);

  const open = useCallback((e) => {
    setOffset(_getContentEasing(-_width, -_width));
    setOpened((_opened = true));
  }, []);

  const [movable] = usePreventActions(onCloseSwipe);

  const bind = useGesture(
    {
      onAction: ({
        down,
        up,
        first,
        event,
        direction: [directionX],
        delta: [deltaX],
        ...rest
      }) => {
        if (!down) {
          const needOpen = Math.abs(Math.min(deltaX, 0)) > _width / 2;

          if (needOpen) {
            open();
            return;
          }
          close();
          return;
        }
        if (directionX == 0) {
          return;
        }
        if (!first) {
          event.preventDefault();
        }
        setOffset(_getContentEasing(Math.min(deltaX, 0), -_width));
      },
    },
    { event: { passive: false } }
  );

  return (
    <SwipeableWrapper>
      <SwipeableActions ref={movable} onClick={close}>
        {actions}
      </SwipeableActions>
      <SwipeableContainer
        {...bind()}
        opacity={Math.abs(range(0, Math.abs(_width), offset))}
        offset={offset}
      >
        {children}
      </SwipeableContainer>
      <TapToReveal onClick={open} open={opened} />
    </SwipeableWrapper>
  );
};

export default Swipeable;

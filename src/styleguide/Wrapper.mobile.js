import React, { useEffect } from 'react';
import Wrapper from './Wrapper';

const WrapperDelegator = (props) => {
  useEffect(() => {
    document.body.classList.add('demo-lg');
    return () => document.body.classList.remove('demo-lg');
  });

  return <Wrapper mobile {...props} />;
};
export default WrapperDelegator;

import React, { Component } from 'react';
import styled from 'styled-components';
import { Theme } from '@src/theme';
import ThemeProvider from '@src/components/Common/ThemeProvider';

const Demo = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 240px;
  background: ${({ theme }) => theme.graySwatches[800]};
  position: relative;
  transform: translate(0, 0);

  .demo-lg & {
    max-width: 360px;
  }
`;
// if demo is true, we can toggle the demo-lg class of demo-body to view the component in landscape or without
// if demo is false, component will restyle when breakpoint is reached (minWidth 300px)
export default class Wrapper extends Component {
  render() {
    const theme = new Theme(true);
    return (
      <ThemeProvider theme={theme}>
        <Demo className={`demo-body`}>{this.props.children}</Demo>
      </ThemeProvider>
    );
  }
}

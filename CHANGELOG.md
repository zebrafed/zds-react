## 0.3.2

######  September 20, 2019

- [Documentation] Align component examples

## 0.3.1

###### _September 11, 2019_

#### Fixes

- [Search] Align autocomplete offsets

## 0.3.0

###### _September 11, 2019_

- [Mobile] Update components and add mobile styles

## 0.2.2

###### _July 16, 2019_

#### Fixes

- [Menu] Fix menu width
- [Search] Remove default placeholder text
- [Drawer] Remove pointer events when closed

## 0.2.1

###### _July 15, 2019_

#### Fixes

- [Dialog] Fix Dialog overlay visibility

## 0.2.0

###### _July 12, 2019_

- [core] Added Drawer component
- [core] Added Search component

## 0.1.2

###### _July 5, 2019_

#### Impovements

- [Swipable] Added click to revile feature
- [Checkbox] Map indeterminate state to native dom input element
- [build] Added `ThemeProvider` and fix module import/exports

## 0.1.1

###### _July 5, 2019_

#### Fixes

- [#46022](https://hive.weareathlon.com/issues/46022) [Card] Title wrong text-size
- [#46023](https://hive.weareathlon.com/issues/46023) [Card] Swipeable card wrong left margin
- [#46024](https://hive.weareathlon.com/issues/46024) [Card] Icon container size wrong dimensions when swipe
- [#46028](https://hive.weareathlon.com/issues/46028) [Dropdown] Type wrong font-weight
- [#46031](https://hive.weareathlon.com/issues/46031) [Dropdown] Dropdown options wrong font-size
- [#46033](https://hive.weareathlon.com/issues/46033) [Dialog] Title wrong font-size
- [#46038](https://hive.weareathlon.com/issues/46038) [Dropdown] Missing icon
- [#46029](https://hive.weareathlon.com/issues/46029) [Dropdown] Arrow icon is different from Zeplin design
- [#46030](https://hive.weareathlon.com/issues/46030) [Dropdown] Swapped font-size (title and text) in leading icon example

## 0.1.0

###### _July 2, 2019_

- [core] Added Card component
- [core] Added Swipeable component
- [core] Added Dropdown component
- [core] Added Checkbox component
- [core] Added Radio component
- [core] Added Switch component
- [core] Added Dialog component
- [core] Added some base components that will help to build up

# Zebra Badger. A React component library.

## Get Started

```bash
$ git clone git@gitlab.athlonsofia.com:zebra/badger-components-react.git
$ cd badger-components-react
$ yarn
```

## Dependencies

- `@zebra/icons` this is icons package that will be used across all platforms - can be installed from https://gitlab.athlonsofia.com/zebra/badger-icons.git or after adding `npm set registry http://npm.kubernetes.athlonsofia.com`

## Development

You can easily develop and interact with your components by using Styleguidist. To run the local server, simply run:

```bash
$ yarn styleguidist
```

Navigate to [http://localhost:6060](http://localhost:6060) to view your styleguidist. They should automatically update as you develop.

Styleguidist will pick up any Readme from the `README.md` file in a component folder.

## Using

This library is build with theme in mind.
You have to put theme provider to use this library

```js
import { ThemeProvider } from '@badger/react';

<ThemeProvider>
  <App />
</ThemeProvider>;
```

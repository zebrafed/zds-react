module.exports = {
  presets: ['@babel/env', '@babel/react', '@babel/flow'],
  plugins: [
    '@babel/plugin-proposal-class-properties',
    'esm' === process.env.BABEL_ENV
      ? '@babel/plugin-transform-modules-systemjs'
      : '@babel/plugin-transform-modules-commonjs',
    ['babel-plugin-webpack-alias', { config: './webpack.config.js' }],
  ],
};
